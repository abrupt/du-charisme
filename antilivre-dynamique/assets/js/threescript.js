import * as THREE from "./three.module.js";

import { GLTFLoader } from "./GLTFLoader.js";
import { DRACOLoader } from "./DRACOLoader.js";
// import { RGBELoader } from "./RGBELoader.js";
// import { FirstPersonControls } from "./FirstPersonControls.js";
// import { TrackballControls } from "./TrackballControls.js";
import { OrbitControls } from "./OrbitControls.js";
// import { OBB } from "./OBB.js";

import { EffectComposer } from "./EffectComposer.js";
import { RenderPass } from "./RenderPass.js";
import { GlitchPass } from "./GlitchPass.js";

import { ShaderPass } from "./ShaderPass.js";
// import { BloomPass } from "./BloomPass.js";
// import { UnrealBloomPass } from "./UnrealBloomPass.js";
import { FilmPass } from "./FilmPass.js";
// import { DotScreenPass } from "./DotScreenPass.js";
// import { MaskPass, ClearMaskPass } from "./MaskPass.js";
// import { TexturePass } from "./TexturePass.js";

// import { BleachBypassShader } from "../shaders/BleachBypassShader.js";
// import { ColorifyShader } from "../shaders/ColorifyShader.js";
// import { HorizontalBlurShader } from "../shaders/HorizontalBlurShader.js";
// import { VerticalBlurShader } from "../shaders/VerticalBlurShader.js";
// import { SepiaShader } from "../shaders/SepiaShader.js";
// import { GammaCorrectionShader } from "../shaders/GammaCorrectionShader.js";
import { VignetteShader } from "../shaders/VignetteShader.js";

let container;
let camera, controls, scene, renderer, clock;
let clickable = true;
let movingCamera = false;
let movingCameraEnd = false;
let dirLight, spotLight;
let prince, dirGroup;
let glitchPass, composer, composer2;

let cubeCamera, cubeRenderTarget;

let quadBG, quadMask, renderScene;
const delta = 0.01;

let textureEquirec;
let animClick = false;

let listSolids = [];

const nbObjects = 1;
let objectVisible;
let listObjects = [];
let listPromises = [];
let listGLTF = [];
const textes = [...document.querySelectorAll(".texte")];
const texteContainer = document.querySelector(".textes");
const hoverTitle = document.querySelector(".hover-title");

function randomNb(min, max) {
  // min and max included
  return Math.floor(Math.random() * (max - min + 1) + min);
}

function randomNbFrac(min, max) {
  // min and max included
  return Math.random() * (max - min + 1) + min;
}

function isTouchDevice() {
  return (
    "ontouchstart" in window ||
    navigator.maxTouchPoints > 0 ||
    navigator.msMaxTouchPoints > 0
  );
}

const mouse = new THREE.Vector2();

// const clock = new THREE.Clock();

init();
animate();

function init() {
  initScene();
  initMisc();

  container.appendChild(renderer.domElement);
  // document.body.appendChild(renderer.domElement);

  // postprocessing

  // const shaderBleach = BleachBypassShader;
  // const shaderSepia = SepiaShader;
  const shaderVignette = VignetteShader;

  // const effectBleach = new ShaderPass(shaderBleach);
  // const effectSepia = new ShaderPass(shaderSepia);
  const effectVignette = new ShaderPass(shaderVignette);
  // const gammaCorrection = new ShaderPass(GammaCorrectionShader);

  // effectBleach.uniforms["opacity"].value = 0.95;
  //
  // effectSepia.uniforms["amount"].value = 0.9;

  effectVignette.uniforms["offset"].value = 0.9;
  effectVignette.uniforms["darkness"].value = 1.5;

  // const effectBloom = new BloomPass(0.5);
  const effectFilm = new FilmPass(0.4, 0.03, 600, false);
  // const effectFilmBW = new FilmPass(0.35, 0.5, 2048, true);
  // const effectDotScreen = new DotScreenPass(new THREE.Vector2(0, 0), 0.5, 0.8);

  // const effectHBlur = new ShaderPass(HorizontalBlurShader);
  // const effectVBlur = new ShaderPass(VerticalBlurShader);
  // effectHBlur.uniforms["h"].value = 1;
  // effectVBlur.uniforms["v"].value = 1;

  // const effectColorify1 = new ShaderPass(ColorifyShader);
  // const effectColorify2 = new ShaderPass(ColorifyShader);
  // effectColorify1.uniforms["color"] = new THREE.Uniform(
  //   new THREE.Color(1, 0.8, 0.8)
  // );
  // effectColorify2.uniforms["color"] = new THREE.Uniform(
  //   new THREE.Color(1, 0.75, 0.5)
  // );
  //
  // const clearMask = new ClearMaskPass();
  // const renderMask = new MaskPass(scene, camera);
  // const renderMaskInverse = new MaskPass(scene, camera);
  //
  // renderMaskInverse.inverse = true;

  //

  // const rtParameters = {
  //   stencilBuffer: true,
  // };

  //

  // const renderBackground = new RenderPass(scene, camera);
  // const renderModel = new RenderPass(scene, camera);

  // Glitch
  composer = new EffectComposer(renderer);
  composer.addPass(new RenderPass(scene, camera));

  glitchPass = new GlitchPass();
  composer.addPass(glitchPass);
  composer.addPass(effectFilm);
  composer.addPass(effectVignette);

  // Permanent
  composer2 = new EffectComposer(renderer);
  composer2.addPass(new RenderPass(scene, camera));

  composer2.addPass(effectFilm);
  composer2.addPass(effectVignette);
  // composer2.addPass(renderBackground);
  // composer2.addPass(renderModel);
  // composer2.addPass(renderMaskInverse);
  // composer2.addPass(effectHBlur);
  // composer2.addPass(effectVBlur);
  // composer2.addPass(clearMask);
  // composer2.addPass(renderScene);
  // composer2.addPass(gammaCorrection);
  // composer2.addPass(effectDotScreen);
  // composer2.addPass(renderMask);
  // composer2.addPass(effectColorify1);
  // composer2.addPass(clearMask);
  // composer2.addPass(renderMaskInverse);
  // composer2.addPass(effectColorify2);
  // composer2.addPass(clearMask);
  // composer2.addPass(effectVignette);

  // composer2.addPass(renderScene);
  // composer2.addPass(gammaCorrection);
  // composer2.addPass(effectSepia);
  // composer2.addPass(effectFilm);
  // composer2.addPass(effectVignette);

  // composer2.addPass(renderScene);
  // composer2.addPass(effectBleach);
  // composer2.addPass(gammaCorrection);

  window.addEventListener("resize", onWindowResize);
}

function initScene() {
  camera = new THREE.PerspectiveCamera(
    45,
    window.innerWidth / window.innerHeight,
    1,
    1000
  );
  camera.position.set(40, 40, 40);

  scene = new THREE.Scene();
  scene.background = new THREE.Color(0x391d81);
  scene.fog = new THREE.Fog(0x391d81, 30, 100);
  // gradient.addColorStop( 0.0, '#743fab' );
  // gradient.addColorStop( 1.0, '#311a46' );
  // gradient.addColorStop( 0.0, '#ff8cac' );
  // gradient.addColorStop( 0.5, '#fb2ead' );
  // gradient.addColorStop( 1.0, '#dc1c9e' );

  // Lights

  scene.add(new THREE.AmbientLight(0x543ba3));

  spotLight = new THREE.SpotLight(0xfb2ead);
  spotLight.angle = Math.PI / 5;
  spotLight.penumbra = 0.3;
  spotLight.position.set(5, 10, 5);
  spotLight.castShadow = true;
  spotLight.shadow.camera.near = 8;
  spotLight.shadow.camera.far = 200;
  spotLight.shadow.mapSize.width = 512;
  spotLight.shadow.mapSize.height = 512;
  spotLight.shadow.bias = -0.002;
  spotLight.shadow.radius = 4;
  scene.add(spotLight);

  dirLight = new THREE.DirectionalLight(0x96bbe8);
  dirLight.position.set(-15, 22, 17);
  dirLight.castShadow = true;
  dirLight.shadow.camera.near = 0.1;
  dirLight.shadow.camera.far = 500;
  dirLight.shadow.camera.right = 35;
  dirLight.shadow.camera.left = -35;
  dirLight.shadow.camera.top = 35;
  dirLight.shadow.camera.bottom = -35;
  dirLight.shadow.mapSize.width = 512;
  dirLight.shadow.mapSize.height = 512;
  dirLight.shadow.radius = 4;
  dirLight.shadow.bias = -0.0005;

  dirGroup = new THREE.Group();
  dirGroup.add(dirLight);
  scene.add(dirGroup);

  // new RGBELoader().setPath("./img/").load("fond.hdr", function (texture) {
  //   texture.mapping = THREE.EquirectangularReflectionMapping;
  //
  //   scene.background = texture;
  //   scene.environment = texture;
  // });

  // cubeRenderTarget = new THREE.WebGLCubeRenderTarget(256);
  // cubeRenderTarget.texture.type = THREE.HalfFloatType;
  //
  // cubeCamera = new THREE.CubeCamera(1, 1000, cubeRenderTarget);

  // Geometry

  // const geometry = new THREE.TorusKnotGeometry(25, 8, 100, 20, 1, 3);
  // const geometry = new THREE.TorusKnotGeometry(40, 8, 3, 20, 4, 1);
  // const geometry = new THREE.SphereGeometry(2, 128, 128);
  // const geometry = new THREE.BoxGeometry( 10, 10, 10 );
  const geometry = new THREE.CapsuleGeometry(1, 3, 8, 64);
  const material = new THREE.MeshPhysicalMaterial({
    color: 0xffffff,
    // color: 0xeac8ff,
    // envMap: cubeRenderTarget.texture,
    roughness: 1,
    metalness: 0,
    // reflectivity: 0,
    // specular: 0x222222,
    // shininess: 1,
    // clearcoat: 0,
    // clearcoatRoughness: 0,
    // emissive: 0x391d81,
  });

  prince = new THREE.Mesh(geometry, material);
  prince.scale.multiplyScalar(1 / 2);
  prince.position.x = 0;
  prince.position.y = 3.2;
  prince.position.z = 0;
  prince.castShadow = true;
  prince.receiveShadow = true;
  scene.add(prince);

  const loader = new GLTFLoader().setPath("./models/");

  const dracoLoader = new DRACOLoader();
  dracoLoader.setDecoderPath("./js/libs/draco/");
  loader.setDRACOLoader(dracoLoader);

  // for (let i = 1; i <= nbObjects; i++) {
  //   let obj = "scene.gltf";
  //   listGLTF.push(obj);
  // }
  //
  // function loadModel(url) {
  //   return new Promise((resolve) => {
  //     loader.load(url, resolve);
  //   });
  // }
  //
  // for (let i = 0; i < nbObjects; i++) {
  //   listPromises[i] = loadModel(listGLTF[i]).then((result) => {
  //     listObjects[i] = result.scene.children[0];
  //   });
  // }
  //
  // Promise.all(listPromises).then(() => {
  //   listObjects.forEach((el) => {
  //     el.scale.divideScalar(2);
  //     el.position.set(0, 0, 0);
  //     // el.visible = false;
  //     // scene.add(el);
  //   });
  //   objectVisible = listObjects[randomNb(0, nbObjects - 1)];
  //   scene.add(objectVisible);
  //   // listObjects[randomNb(0,7)].visible = true;
  //   const finish = document.querySelector(".loading");
  //   finish.classList.add("loading--obj-loaded");
  // });

  let grid = [];
  const columns = 6;
  const rows = 6;
  const sizeCell = 7;
  for (let i = 0; i < rows; i++) {
    for (let j = 0; j < columns; j++) {
      grid.push({
        occupy: 0,
        x: j * sizeCell - Math.floor(columns / 2) * sizeCell,
        y: i * sizeCell - Math.floor(rows / 2) * sizeCell,
      });
    }
  }

  const nbSolids = textes.length;
  const cylinderGeometry = new THREE.ConeGeometry(0.75, 7, 64);
  cylinderGeometry.rotateX(Math.PI / 2);

  const distance = 10;
  for (let i = 0; i < nbSolids; i++) {
    // const obj = new THREE.Mesh(cylinderGeometry, material);
    loader.load("courtisan.glb", function (gltf) {
      const nbCell = randomNb(0, grid.length - 1);
      const cell = grid[nbCell];
      let posX = cell.x;
      let posZ = cell.y;
      const obj = gltf.scene;
      obj.scale.divideScalar(7);
      // obj.rotateZ(Math.PI / 2);
      obj.position.set(posX, 2.5, posZ);

      // obj.castShadow = true;
      // obj.receiveShadow = true;
      obj.traverse(function (child) {
        if (child.isMesh) {
          child.castShadow = true;
          child.receiveShadow = true;
          child.name = i;
        }
      });
      grid.splice(grid.indexOf(cell), 1);

      obj.name = i;
      scene.add(obj);
      listSolids.push(obj);

      renderer.render(scene, camera);
    });
    if (i == nbSolids - 1) {
      const finish = document.querySelector(".loading");
      finish.classList.add("loading--all-is-loaded");
    }

    // object.position.set(posX, 3.5, posZ);
    // object.position.y = 3.5;
    // object.position.z = randomNb(-distance, distance);

    // object.castShadow = true;
    // object.receiveShadow = true;
    // grid.splice(grid.indexOf(cell), 1);
    //
    // object.name = i;

    // listSolids.push(object);
    // scene.add(object);
  }

  const planeGeometry = new THREE.PlaneGeometry(200, 200);
  const planeMaterial = new THREE.MeshPhongMaterial({
    color: 0x999999,
    shininess: 1,
    specular: 0x111111,
  });

  const ground = new THREE.Mesh(planeGeometry, planeMaterial);
  ground.rotation.x = -Math.PI / 2;
  ground.scale.multiplyScalar(3);
  ground.castShadow = true;
  ground.receiveShadow = true;
  scene.add(ground);
}

function initMisc() {
  container = document.getElementById("container");
  renderer = new THREE.WebGLRenderer({ antialias: true });
  renderer.setPixelRatio(window.devicePixelRatio);
  renderer.setSize(window.innerWidth, window.innerHeight);
  // renderer.outputEncoding = THREE.sRGBEncoding;
  // renderer.toneMapping = THREE.ACESFilmicToneMapping;
  // renderer.toneMappingExposure = 2.1;
  renderer.shadowMap.enabled = true;
  renderer.shadowMap.type = THREE.VSMShadowMap;

  // Mouse control
  controls = new OrbitControls(camera, renderer.domElement);
  controls.maxPolarAngle = Math.PI / 2;
  controls.enableDamping = true;
  // controls.minDistance = 1;
  controls.enablePan = false;
  controls.maxDistance = 100;
  controls.target.set(0, 2, 0);
  controls.update();

  clock = new THREE.Clock();
}

// let timer;
controls.addEventListener("start", (el) => {
  movingCameraEnd = false;
  // timer = setTimeout(() => {
  //   moveControls = true;
  // }, 150);
});

controls.addEventListener("end", (el) => {
  // clearTimeout(timer);
  // clickable = false;
  // if (!clickable) {
  //   moveChangeControls = false;
  // }
  movingCameraEnd = true;
  if (movingCamera) {
    movingCamera = false;
  }
});

controls.addEventListener("change", (el) => {
  if (movingCameraEnd) {
    movingCamera = false;
  } else {
    setTimeout(() => {
      movingCamera = true;
    }, 200);
  }
});

container.addEventListener("click", onDocumentMouseClick, false);
container.addEventListener("mousemove", onDocumentMouseMove, false);
texteContainer.addEventListener(
  "mousemove",
  (event) => {
    event.stopPropagation();
    document.body.style.cursor = "";
    hoverTitle.classList.remove("hover-title--show");
    hoverTitle.innerHTML = "";
  },
  false
);

let textesOpen = false;
function onDocumentMouseClick(event) {
  const mouse = new THREE.Vector2();
  mouse.x = (event.clientX / window.innerWidth) * 2 - 1;
  mouse.y = -(event.clientY / window.innerHeight) * 2 + 1;
  let raycaster = new THREE.Raycaster();
  raycaster.setFromCamera(mouse, camera);
  const intersects = raycaster.intersectObjects(listSolids, true);
  const intersectPrince = raycaster.intersectObject(prince, false);
  if (intersectPrince.length > 0 && clickable) {
    animClick = true;
    setTimeout(() => {
      animClick = false;
    }, 5000);
  }
  if (intersects.length > 0 && clickable) {
    const texteChoisi = textes[intersects[0].object.name];
    hoverTitle.classList.remove("hover-title--show");
    document.body.style.cursor = "";
    hoverTitle.innerHTML = "";
    if (textesOpen) {
      textes.forEach((el) => {
        el.classList.remove("show");
      });
      texteChoisi.classList.add("show");
      document.querySelector(".texte.show").scrollIntoView();
    } else {
      texteContainer.classList.add("show");
      texteChoisi.classList.add("show");
      // if (!isTouchDevice()) controls.activeLook = false;
      textesOpen = !textesOpen;
    }
  }
}

const closeBtn = document.querySelector(".textes__close");
closeBtn.addEventListener("click", (e) => {
  e.preventDefault();
  document.querySelector(".texte.show").scrollIntoView();
  texteContainer.classList.remove("show");
  textes.forEach((el) => {
    el.classList.remove("show");
  });
  textesOpen = !textesOpen;
  // if (!isTouchDevice()) controls.activeLook = true;
  closeBtn.blur();
});

function onDocumentMouseMove(event) {
  mouse.x = (event.clientX / window.innerWidth) * 2 - 1;
  mouse.y = -(event.clientY / window.innerHeight) * 2 + 1;
  let raycaster = new THREE.Raycaster();
  raycaster.setFromCamera(mouse, camera);
  const intersects = raycaster.intersectObjects(listSolids, true);
  const intersectPrince = raycaster.intersectObject(prince, false);
  if (intersects.length > 0 || intersectPrince.length > 0) {
    document.body.style.cursor = "pointer";
    if (intersects.length > 0) {
      hoverTitle.classList.add("hover-title--show");
      const texteChoisi = textes[intersects[0].object.name];
      const title = texteChoisi.querySelector("h1");
      hoverTitle.innerHTML = title.innerHTML;
    }
  } else {
    hoverTitle.classList.remove("hover-title--show");
    document.body.style.cursor = "";
    hoverTitle.innerHTML = "";
  }
}

function onWindowResize() {
  camera.aspect = window.innerWidth / window.innerHeight;
  camera.updateProjectionMatrix();

  renderer.setSize(window.innerWidth, window.innerHeight);

  // controls.handleResize();
}

function animate(time) {
  requestAnimationFrame(animate);
  // if (isTouchDevice()) controls.update();
  // cubeCamera.update(renderer, scene);
  controls.update(); // only required if controls.enableDamping = true, or if controls.autoRotate = true
  renderer.render(scene, camera);

  if (movingCamera) {
    clickable = false;
  } else {
    clickable = true;
  }

  const delta = clock.getDelta();

  prince.rotation.x += 0.25 * delta;
  prince.rotation.y += 0.5 * delta;
  prince.rotation.z += 1 * delta;

  const timeOther = Date.now() * 0.0005;

  prince.position.x = Math.sin(timeOther * 0.7) * 20;
  // prince.position.y = Math.cos(timeOther * 0.5) * 2000;
  prince.position.z = Math.cos(timeOther * 0.3) * 20;

  for (let i = 0; i < listSolids.length; i++) {
    const solid = listSolids[i];
    if (!animClick) {
      solid.lookAt(prince.position);
      // solid.rotation.z = Math.atan2( ( prince.position.x - solid.position.x ), ( prince.position.z - solid.position.z ) );
    } else {
      const randomNumb = randomNb(1, 9);
      solid.rotation.x += randomNumb * delta;
      solid.rotation.y = 0;
      solid.rotation.z += randomNumb * delta;
    }
  }
  if (animClick) {
    composer.render();
  } else {
    composer2.render();
  }

  spotLight.target = prince;
  // spotLight.target.position.z = prince.position.z;

  // dirGroup.rotation.y += 0.7 * delta;
  // dirLight.position.z = 17 + Math.sin(time * 0.001) * 5;

  // if (!isTouchDevice()) controls.update( clock.getDelta() );
}

// Scripts

// Correct 100 Vh - Source : https://codepen.io/team/css-tricks/pen/WKdJaB
// First we get the viewport height and we multiple it by 1% to get a value for a vh unit
let vh = window.innerHeight * 0.01;
// Then we set the value in the --vh custom property to the root of the document
document.documentElement.style.setProperty("--vh", `${vh}px`);

// We listen to the resize event
window.addEventListener("resize", () => {
  // We execute the same script as before
  let vh = window.innerHeight * 0.01;
  document.documentElement.style.setProperty("--vh", `${vh}px`);
});

// Prevent bounce effect iOS
// Source https://gist.github.com/swannknani/eca799795860cff222f70b8675f8c8d8
// var contentBounce = document.querySelector('.container');
// contentBounce.addEventListener('touchstart', function (event) {
//   this.allowUp = this.scrollTop > 0;
//   this.allowDown = this.scrollTop < this.scrollHeight - this.clientHeight;
//   this.slideBeginY = event.pageY;
// });

// contentBounce.addEventListener('touchmove', function (event) {
//   var up = event.pageY > this.slideBeginY;
//   var down = event.pageY < this.slideBeginY;
//   this.slideBeginY = event.pageY;
//   if ((up && this.allowUp) || (down && this.allowDown)) {
//     event.stopPropagation();
//   } else {
//     event.preventDefault();
//   }
// });

//// Menu
const menuBtn = document.querySelector(".btn--menu");
const menu = document.querySelector(".menu");
const morceau = document.querySelector(".son");
const musiqueBtn = document.querySelector(".btn--sound");
const musiqueBtnOn = document.querySelector(".btn--sound .info--on");
const musiqueBtnOff = document.querySelector(".btn--sound .info--off");
let menuOpen = false;

menuBtn.addEventListener("click", (e) => {
  e.preventDefault();
  menuOpen = !menuOpen;
  if (menuOpen) {
    // if (!isTouchDevice()) controls.activeLook = false;
  } else {
    // if (!isTouchDevice()) controls.activeLook = true;
  }
  menu.classList.toggle("show--menu");
  menuBtn.blur();
});

// Musique
// var musiqueJoue = "pause";
let vol = 0;
let interval = 30;
let fadeInAudio;
let fadeOutAudio;
function musique(track) {
  if (track.paused) {
    clearInterval(fadeInAudio);
    clearInterval(fadeOutAudio);
    track.volume = vol;
    track.play();
    fadeInAudio = setInterval(function () {
      if (track.volume < 0.99) {
        track.volume += 0.01;
      } else {
        clearInterval(fadeInAudio);
      }
    }, interval);
  } else {
    track.pause();
  }
}
let silence = true;
musiqueBtn.addEventListener("click", (e) => {
  e.preventDefault();
  if (silence) {
    musique(morceau);
  } else {
    clearInterval(fadeInAudio);
    clearInterval(fadeOutAudio);
    morceau.pause();
  }
  musiqueBtnOff.classList.toggle("none");
  musiqueBtnOn.classList.toggle("none");
  silence = !silence;
  musiqueBtn.blur();
});

//
// Scrollbar with simplebar.js
//
let simpleBarContent = new SimpleBar(document.querySelector(".textes"));
// Disable scroll simplebar for print
window.addEventListener("beforeprint", (event) => {
  simpleBarContent.unMount();
});
window.addEventListener("afterprint", (event) => {
  simpleBarContent = new SimpleBar(document.querySelector(".textes"));
});

// // Anciennes versions
// let messageIOS = false;
//
// const platform = window.navigator.platform;
// const iosPlatforms = ['iPhone', 'iPad', 'iPod'];
//
// if (!messageIOS && iosPlatforms.indexOf(platform) !== -1) {
//   const banner = document.createElement('div');
//   banner.innerHTML = `<div class="devicemotion--ios" style="z-index: 900; position: absolute; bottom: 0; left: 0; width: 100%; background-color:#000; color: #fff;"><p style="font-style: 1em; padding: 2em 1em; text-align: center;">Cet antilivre risque de ne pas fonctionner avec des versions d’iOS inférieures à la version 13.<br>(Cliquer sur cette bannière pour la faire disparaître).</p></div>`;
//   banner.onclick = () => banner.remove(); // You NEED to bind the function into a onClick event. An artificial 'onClick' will NOT work.
//   document.querySelector('body').appendChild(banner);
//   messageIOS = true;
// }
