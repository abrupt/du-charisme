```{=tex}
\vspace*{6\baselineskip}
\thispagestyle{empty}
\epigraphe{La vie de la cour est un jeu sérieux, mélancolique, qui applique.}{Jean de La Bruyère}{}
```
```{=tex}
\cleardoublepage
```
# Avant-propos

`\textsc{Comme tout le monde}`{=tex}, j'imagine, il m'est arrivé de croiser la route d'êtres charismatiques : professeurs, maîtres à penser, écrivains, artistes de toutes sortes, dont la personnalité exerce un charme, une séduction intellectuelle aussi passagère qu'envoûtante. Certains étaient coiffés d'une juste couronne de prestige, d'autres, hélas plus communs, l'avaient usurpée. Pourtant l'un comme l'autre, ils se prévalaient en toute chose du rang qui leur était dû.

Les années passant, il m'a semblé devoir saluer le talent d'escroc de ceux que j'appellerai ici, pour les tourner en dérision, des "princes" par analogie avec la position occupée par les "grands" dans la société d'Ancien Régime. Même s'il est évident que tous ces princes de carton dont il sera question ne disposent d'aucun pouvoir tangible, je veux jouer avec cette ambiguïté, en explorer la fécondité pour tenter de décrire ce si particulier rapport de sujétion symbolique.

Je voudrais donc réfléchir à la logique oppressive du groupe, aux effets de discours nécessaires à l'existence de "cercles" qui sont les répliques inconscientes ou naturelles (parce que dans la sociabilité de groupe il n'en va jamais autrement) des "sociétés de cour", aux conditions d'émergence de la figure du prince, au vocabulaire dans lequel s'énonce la soumission, aux signes d'allégeance. Je voudrais détailler un peu les techniques mises à l'œuvre --- si l'on m'accorde que la séduction est aussi affaire de techniques --- pour restreindre l'accès à sa personne et forger par la sélection qu'il impose un sentiment d'appartenance.

Comment le prince prend-il l'ascendant ? Par où, par quels moyens fascine-t-il ? Comment emporte-t-il les avis ?

J'aimerais saisir, en sa genèse, la naissance immatérielle de ce qu'on appelle "charisme" sans trop savoir ce qu'on désigne généralement par là, sa fabrique factice, éclairer, comme par dessous, les facettes de cette si sérieuse comédie du prestige.

J'aimerais montrer comment le "courtisan" qui forme la société immédiate et le premier entourage du prince s'empresse à faire plaisir pour que ce dernier prenne confiance en lui et comment, une fois introduit, il s'use à prévenir les désirs du prince en déchiffrant ses goûts. Et le montrer à l'œuvre dans un contexte contemporain.

Car même dans une société qui incline à la transparence et d'où l'esprit de cour devrait avoir totalement disparu, les techniques par lesquelles s'obtenaient jadis la grâce ou la disgrâce sociales se réactivent spontanément chaque fois que, dans un groupe, apparaît un centre. Un centre ? Oui, cette classe de gens reconnaissables entre tous. "Ils s'arrêtent, et on les entoure" (La Bruyère).

De telles manifestations de soumission collective s'observent quotidiennement. Il me semble pourtant qu'elles subsistent de façon anachronique et que seules des situations qui perpétuent sciemment l'observance de ce jeu archaïque, ayant intérêt à le faire, sont encore capables de susciter ce type de comportement.

Enfin, pas de prince sans "cour".

Il serait de mauvais aloi de négliger l'étude de la nature réelle des compétences et des services rendus par cette foule de satellites, loyale et docile, dont l'unique souci semble se résumer à ne jamais rien oser, à toujours donner contentement, mais qui entre rarement par accident dans le giron du prince. N'étant le fait ni du hasard, ni de la naissance, ni des titres, cette soumission doit suivre quelque logique, même obscure. Je supposerai donc que le courtisan lui aussi se livre dans l'ombre du prince à une activité de contrebande. Laquelle ? Il s'agira de l'apprendre.

# Introduction

`\textsc{On fait}`{=tex} généralement dépendre la question du charisme de celle de l'autorité, en la traitant comme un sous-domaine des problèmes que pose l'incarnation de l'autorité dans une personne, ainsi que la soumission volontaire qu'elle induit.

Mais aussitôt se dresse une série d'écueils. Le domaine de l'autorité semble toujours hérissé de mystères.

Or le mystère est sans "comment", sans origine ; une évidence obscure, un éblouissement qui voile sa source. Certains assument le pouvoir, d'autres le délaissent ; certains dominent, d'autres se soumettent ; certains se démarquent, d'autres s'effacent : l'ébahissement demeure. Les causes mêmes de ce partage nous laissent courts. Naturellement, d'instinct, servitude et pouvoir forment l'avers et le revers d'une même médaille.

Dès qu'un groupement s'agrège, dès qu'une réunion s'organise, deux camps se forment. Meneurs et suiveurs, princes et courtisans se rangent aussitôt à leur place, la leur depuis le commencement. On dira : n'est-ce pas la preuve qu'une loi organise cette répartition régulière ? une loi archaïque qui nous relie aux anciennes hordes ? une règle qui distribue par lots ses cases vacantes ? Le constat d'une récurrence ne procure cependant ni explication ni théorie.

Au fondement de l'autorité, on ne trouve rien : ni lien, ni lieu, ni loi.

C'est donc cette absence qu'il faut commencer par penser. Si, d'un côté, l'autorité de celui qui "fait" autorité dans un domaine existe parce qu'il accepte de se soumettre de manière régulière à des épreuves de vérité (rite, match, concours, publication, etc.) et de donner ainsi des preuves constamment renouvelées de sa compétence, l'autorité de celui qui "a" autorité surgit de manière bien plus déconcertante, presque naturellement, en l'absence de procédures stables. Là où l'autorité de type "institué" advient dans une culture de la transparence et de la preuve, l'autorité de type "charismatique", celle qui caractérise les princes, advient sans trop que l'on sache (dire) pourquoi. Pourtant cette absence de fondement n'est pas un mystère. C'est au contraire la trace la plus évidente de la présence quelque part, à un moment quelconque, d'un coup de bluff. Comme tout escamoteur, l'être charismatique n'est pas assez stupide pour dévoiler les manœuvres lui permettant de conquérir le peu de prestige dont il se targue. Il sait dissimuler son art le mieux du monde.

J'aimerais ainsi montrer que l'autorité dont se réclame le prince existe, mais seulement comme jeu, sauvage, effréné, sans règle, improvisé au milieu des procédures régulières d'attribution de la compétence, et que seule la probabilité d'augmenter un avantage acquis d'improbable façon pousse ces joueurs à y jouer, selon un calcul purement spéculatif.

Lorsque Étienne de La Boétie étudia le problème de la soumission à l'autorité sans usage de la force, il produisit une théorie fondée sur le principe du ruissellement des faveurs par laquelle il expliquait l'attachement de la base au sommet, soit l'asservissement de la multitude à l'Un, par l'établissement d'un système pyramidal d'octroi de privilèges et de protections.

Dans cette théorie, un scénario imaginaire tient lieu d'explication. Ce scénario raconte comment le puissant, grâce aux gains "profitables" qu'il octroie, tient sous lui cinq ou six "compagnons", des "complices" qui à leur tour asserviront les uns au moyen des autres.

Aux yeux de La Boétie, ce microrécit démontre trois choses quant au fondement de l'autorité : il n'y a pas d'autorité sans escompte de "profit" ; pas d'autorité sans accumulation de "petits moyens" ; pas d'autorité sans tromperie à "bon marché".

Ce système à trois éléments (volonté d'accroître sa valeur, capitalisation de microavantages, contournement de l'exigence de preuve) reste pertinent pour l'étude du "charisme", même si le cadre d'analyse est sensiblement différent.

On ne peut, sans raccourci ou caricature, assimiler le prince au tyran de *La Servitude volontaire*. Car si l'individu charismatique fait effectivement "le plus grand tort à la liberté", c'est d'abord et surtout à la liberté de penser.

Dans ce qui suit, il sera donc beaucoup question de "liberté de jugement" et peu de "liberté d'action". Contrairement au siècle de La Boétie, le périmètre de nos libertés résulte avant tout de la qualité du débat d'idées dans l'espace public. Pour nous, contemporains, la liberté d'agir est soumise à l'indépendance de penser. Nul doute, en effet, que la puissance ne se trouve aujourd'hui du côté de ceux qui savent influencer l'opinion.

Par souci de transparence, j'indique que le matériel de réflexion provient pour une large part des portraits et des tableaux compilés par le chroniqueur le plus assidu de la vie de cour d'Ancien Régime, je veux parler de Saint-Simon. Certaines notions lui sont directement empruntées (voir Glossaire). La méthode a simplement consisté à les décontextualiser pour les transplanter dans un terreau contemporain où ils auront formé, je l'espère, de nouvelles racines. J'ai donc fait le pari d'analyser le phénomène familier du "charisme" avec des mots du passé pour manifester son anachronisme.

J'ajoute que deux hypothèses inspirées de La Boétie sous-tendent de bout en bout cet essai. La première est que la genèse de l'individu charismatique suit une courbe inflationniste : de presque rien, il finit par concentrer autour de sa personne presque tout. Il sera ainsi question de "capitalisation" et d'"accumulation" --- chacun se sentira libre de s'emparer ou non de la portée politique de ces termes.

La seconde est que l'exigence de preuve et de transparence qui définissent la vertu intellectuelle fait paradoxalement le lit des princes. Parce qu'il est coûteux de soumettre le savoir à des règles de validation, parce qu'il faut faire des efforts pour se conformer à ces procédures d'établissement de la vérité, ces règles et ces procédures contiennent en elles-mêmes une incitation à les contourner.

Toute la difficulté, pour moi, a consisté à produire une démonstration articulée, alors que le phénomène observé est de nature "holistique". J'aurais maintes fois l'occasion de répéter que le prince évolue dans un système où chaque élément renvoie à un autre élément du même système, et que sa possibilité d'existence tire précisément sa force de sa capacité à faire croire à la réalité de cette possibilité.

# Autorité, opinion, adhésion

`\textsc{Avant de tenter}`{=tex} de comprendre d'où viennent l'autorité dite "naturelle" de l'individu charismatique et les effets de fascination qu'elle occasionne, il faut rapidement présenter les trois principales familles théoriques proposant un modèle de ce type d'autorité et inscrire notre démarche dans ce cadre élargi.

Ainsi, parmi les conceptions les plus courantes de l'autorité, on distingue : (1) une conception "imputative" suivant laquelle le pouvoir permettant d'imposer des choix réside dans la puissance d'assertion de celui qui les affirme : le groupe "prolonge" l'action du chef charismatique qui impose la conviction de son "don" de prendre toujours des "décisions heureuses" (Elias, Kojève) ; (2) une conception "approbative" selon laquelle, "séduits par les apparences", les petits esprits défèrent leur pouvoir de décider ce qu'il convient de faire ou de penser au "grand" qui les énonce (La Boétie, Montesquieu) ; enfin, (3) une conception "mystique" qui rejette "hors de portée de l'analyse" toute interrogation sur ses fondements (Montaigne, Derrida, Menger).

Aucune de ces conceptions dont la formulation a été réduite ci-dessus à son noyau n'est entièrement satisfaisante, car la logique qui fonde ces rapports reste, elle, inexpliquée, et le restera aussi longtemps qu'on n'examinera pas les raisons d'agir que ces rapports sous-tendent.

La piste mixte (3), qui combine les positions (1) et (2), semble la plus prometteuse, à condition toutefois de clarifier les termes de cette articulation qui lie l'approbation à l'imputation d'autorité. C'est ce que je vais m'efforcer de faire en commençant par spécifier le genre d'individu qu'est le prince.

Tentons un premier pas : "puissant" serait celui qui peut dire "Je trouve mauvais que...", s'autorisant publiquement à faire connaître son appréciation. Par la liberté qu'il prend, par la sidération, même minime, que produit sa saillie, il semble qu'il parvienne à modifier, selon un tropisme qui reste à décrire, la perception qu'a son entourage de l'action, de la personne ou de l'objet qu'il évalue de la sorte.

Il y a incontestablement dans le fait de "s'autoriser" à dire ou à faire quelque chose un ingrédient psychologique. Car il faut, pour s'autoriser à évaluer subjectivement quelque chose en public, se croire --- à tort ou à raison --- immunisé contre la critique d'autrui. Autrement dit, la croyance en la fiabilité de ses propres jugements incite mécaniquement à produire de tels jugements, sans pour autant qu'ils soient objectivement fiables ou robustes.

Ici, l'insensibilité à la critique d'autrui --- c'est-à-dire l'acceptation indifférente de la critique --- doit être considérée comme un cas particulier de la croyance erronée en la solidité de son propos : s'il vous est égal que vos propos soient (possiblement) critiqués, si vous acceptez de sacrifier (possiblement) votre fierté, s'il vous est indifférent d'attirer (possiblement) la haine, c'est que vous croyez à ce que vous dites, mais que vous vous trompez en le croyant. Vous vous êtes armé de courage, vous avez voulu croire que votre esprit ne pouvait pas être vaincu, alors qu'en réalité vos idées étaient défectueuses ou vulnérables.

Prenons un exemple.

Quand quelqu'un dit "on ne compte pas les hommes" pour insister, mais sous l'angle de l'impératif moral, sur la singularité et l'insubstituabilité de chaque vie humaine, cette personne s'autorise à dire quelque chose de banal sur un ton péremptoire. Il n'y a pas de difficulté à reconnaître que le fondement de son propos est un lieu commun ; mais sous sa forme subjective : "Je trouve mauvais que l'on compte les hommes", l'énoncé prend une subtile tonalité affective. Tout à coup, en plus d'elle-même, l'idée porte la marque d'une sensibilité, d'une pénétration d'esprit qui voudrait signaler une grandeur d'âme.

De telles formules, à la tonalité poético-philosophique, ont la capacité d'ébranler le jugement en combinant deux registres : le passionnel et le rationnel. Et pour peu que le premier l'emporte sur le second, ce genre de phrase sentencieuse peut vite devenir ensorcelante.

Nous appelons mauvais ce qui ne nous plaît pas --- ce qui semble être la seule définition du mauvais qui soit pertinente dans notre contexte. Chez le prince, la réprobation morale (sur le plan des valeurs) est systématiquement articulée à un jugement subjectif (sur le plan du goût).

Dire "on ne compte pas les hommes" signifie "il est inadmissible de compter les hommes parce que ça ne me plaît pas." Le problème, en l'occurrence, ne porte pas sur le contenu de la thèse, mais sur la nature de la caution versée par l'appréciation subjective. Encore que la thèse elle-même mériterait peut-être une discussion.

Il y a en effet de bonnes raisons de penser que "compter les hommes" afin, par exemple, de connaître le taux de fréquentation d'une salle de spectacle soit moralement acceptable.

Bref, la question sous-jacente n'est pas tellement de savoir "pourquoi" ce genre d'évaluation arrive à s'imposer, puisque de fait ces évaluations s'imposent, mais "comment" elles y parviennent, alors même que leur validité n'est pas débattue.

D'ordinaire, dans le cours d'une discussion qui tend à l'élucidation d'une difficulté, chaque avis est ouvert à la controverse jusqu'à la formation d'un consensus (approbation) ou bien d'un différend (rejet).

Lorsque le prince parle, ce n'est pas ce qui se passe. Car quand il cause, sérieusement ou non, cela doit se savoir. Le langage, chez lui, sert d'abord à créer du contact : voyez, dès qu'il s'explique, la qualité de l'attention diffère de manière presque palpable. Tout se passe comme s'il créait une diversion sonore autour de lui. Peu importe, en effet, si le niveau d'étayage de ses opinions (la vérification des faits cités) ou si leur cohérence (la compatibilité des thèses soutenues) est faible : le "bruit" que produisent ses paroles capte l'attention et, au sein du groupe ainsi formé, instaure une forme de familiarité, même si celle-ci dépend seulement de l'occasion.

La question du bruit est intéressante parce qu'elle se rapproche par certains aspects de la problématique de la "foutaise" comme style de parole caractérisée par son indifférence aux normes du savoir (Frankfurt). En effet, tout comme la foutaise, le bruit produit un discours sautant du coq à l'âne, imprécis, vague, discontinu, prétentieux, pseudo-profond dont le seul but est, semble-t-il, d'occuper le terrain.

Mais si le bruit, comme la foutaise, est bien une sorte de langage tout prêt destiné à meubler la conversation avec une exigence minimale quant au savoir, il ne remplit pas exactement la même fonction. La production du bruit est un moyen peu coûteux --- puisqu'il manipule l'objet sonore qu'est aussi le langage --- de former un cercle d'attention autour de soi. Quand une porte claque, on se retourne. Quand quelqu'un "bruisse", c'est la même chose. Le bruit est une émission sonore qui oriente l'attention du groupe. Celui qui parle bascule au premier plan, tandis que ce dont il parle est repoussé à l'arrière-plan. Tout l'intérêt de faire du bruit se loge dans la constatation qu'il est plus facile de faire applaudir ses choix en se faisant remarquer que d'en débattre.

Imaginons maintenant que vous sachiez à l'avance que les avis que vous rendrez seront immanquablement suivis, relayés, repris sans opposition ni contradiction. Ne devrait-on pas dire que vous disposez là d'un grand pouvoir ? Vous pourriez imposer le type d'impression qui amène vos interlocuteurs à agir de leur plein gré conformément à votre propre dessein (Goffman).

Effectivement, avant même de prendre la parole, le "puissant" a en quelque sorte l'assurance d'être cru. Et ce pouvoir, le pouvoir de celui qui dit "je trouve mauvais que...", réside dans sa capacité de créer une opinion identique dans l'esprit de ses auditeurs. En disant cela, il n'exprime pas seulement un point de vue, il rend crédible sa croyance à propos de "ce qu'il trouve mauvais". Ainsi, pour faire croire à la vérité d'un point de vue, il ne faut pas que ce point de vue soit "vrai" (qu'il concorde avec les faits) ; mais seulement "crédible" (que d'autres puissent y adhérer).

En donnant accès à l'expression d'une opinion, le puissant montre, en même temps qu'il l'affirme, qu'il se croit --- à tort ou à raison --- immunisé contre la critique d'autrui.

Contrairement à une idée répandue, il n'est pas donné à tout le monde de pouvoir exprimer une opinion. La sociologie a montré que les gens font d'autant moins connaître leurs opinions qu'ils ne sont pas intéressés par un problème, de sorte que formuler une opinion peut être considéré comme une forme de privilège (Heinich). Le privilège réside en l'occurrence dans la capacité à s'attribuer une forme de compétence autorisant à rendre un avis.

Ainsi l'expression publique d'une opinion revient à la fois à révéler son degré de compétence dans un domaine considéré en même temps qu'à soumettre son interprétation à la concurrence des modèles d'opinion relatifs à ce domaine. Si bien que plus la concurrence entre interprétations est rude ou plus la solidarité autour d'une interprétation est étroite, plus il sera difficile de lancer une nouvelle opinion.

Face à certains modèles d'opinion majoritaires, il devient quasiment impossible d'émettre des opinions discordantes, lesquelles soulèveraient aussitôt la réprobation générale. À l'opposé, une opinion renouvelant l'interprétation d'un problème remplacera avantageusement des alternatives plus faibles sans nécessairement produire une réfutation systématique. Ainsi, une conviction se forme quand on croit l'interprétation d'autrui la meilleure, même si cela ne prouve pas qu'elle l'est objectivement.

L'élégance, en matière de discours, aura toujours infiniment plus de pouvoir de persuasion que le raisonnement le plus pur, tant les hommes se gouvernent davantage par les sentiments que par la raison. Le ressort de l'adhésion est presque toujours affectif : nous sommes portés à croire non par la preuve, remarquait Pascal, mais par l'agrément. Autrement dit, dans le monde commun, lorsque la raison essaie de justifier une croyance, c'est le plus souvent que celle-ci est déjà formée.

On regarde généralement une opinion comme la communication d'une conviction personnelle. Je serais enclin à penser qu'elle s'apparente plutôt à une indiscrétion calculée : comme une porte délibérément entrebâillée sur l'arrière-boutique de nos pensées.

Dans ce cas, exprimer une opinion revient à dévoiler un petit pan du discours qu'on (se) tient sur le monde. Ce discours n'a besoin, bien sûr, ni d'être cohérent ni d'être explicité, bien qu'il vise un idéal de cohérence : c'est le récit intime dans lequel s'ordonnent tant bien que mal, se raboutent toutes les croyances et valeurs qui nous tiennent lieu de boussole en société. Or, il n'y a pas d'obstacle à concevoir que l'accès à ces informations privées et donc rares puisse susciter la convoitise des courtisans et affûter leur appétit.

Il est possible (c'est une possibilité) que je trouve mauvais X parce que dans le monde tel que je le mets en récit "X est mauvais". Pour être cru, il suffirait de convaincre les incrédules d'entrer dans mon récit.

Une interprétation élégante fait toujours plus envie qu'un raisonnement juste mais austère ; or la fascination découle souvent de l'envie : envie "d'en être", de faire partie de ce monde où l'on juge les choses ainsi, ou encore envie d'être celui qui produit ce type d'interprétation. Si l'explicitation des raisons de trouver X mauvais comporte un coût intellectuel, l'identification à un récit où il est désirable de trouver X mauvais est aussi spontanée, aussi naturelle que l'immersion dans une fiction.

Jean de La Fontaine avait déjà pointé cette puissance d'envoûtement du récit, lorsqu'il apprenait aux puissants dans *Le Pouvoir des fables* combien une histoire peut plus qu'un discours (autoritaire ou logique).

Qui peut donc se permettre de prendre la parole sans courir le risque d'être démenti ni craindre d'égratigner ses interlocuteurs ? Qui peut parler sans ménagement ? Réponse : celui qui sait ce qu'il vaut, car son sentiment de grandeur lui confère une immunité face au débat qui pourrait le cas échéant surgir : immunité garantissant qu'il sera en mesure d'assourdir, si elle s'amorce, la controverse ; même si le plus souvent celle-ci n'a pas lieu.

Naturellement, celui qui a la certitude de savoir ce qu'il vaut ne doute jamais de son avantage, qu'il regarde comme "normal" en vertu de sa valeur.

La façon dont un homme est regardé conditionne la manière dont il se regarde : s'il reçoit de manière répétée des marques de considération personnelle, il se formera sans doute aussi une estime parfaite de lui-même.

À l'inverse, s'il est attaqué, contesté, il voudra faire passer la témérité de l'insolent qui pique sa vertu pour un cabotinage.

Remarquons enfin que cet avantage peut être réel ou supposé. On peut être érudit ou se croire érudit : du point de vue de la certitude concernant qui l'on est, il y a peu de différence. Par conséquent, il ne me semble pas excessif d'imaginer que celui qui éprouve pleinement le sentiment de sa grandeur se voit ou se vit comme un prince.

# L'autorité dérivant du talent

`\textsc{Tout individu}`{=tex} qui en impose n'est pas "prince" pour autant, tant s'en faut. Essayons de dissiper quelques malheureuses équivoques.

En dépit de l'apparence qu'il se donne, le prince, au sens figuré qu'il a ici, n'est ni un chef (de file, d'équipe, d'entreprise, etc.) ni une idole. Il se distingue de ces deux "types" qui engendrent aussi parfois, mais pour des raisons différentes, des effets de fascination.

Le chef occupe toujours une position de pouvoir et tire autorité de sa fonction. Son statut protège sa parole de sorte qu'il est à peu près libre de dire n'importe quoi, étant donné qu'il aura toujours la possibilité de faire valoir, en dernier recours, ce statut pour récuser une critique défavorable.

L'idole, vivante ou morte, suscite quant à elle estime et désir, tour à tour proche et inaccessible. Tous les adulateurs le savent à leur grand dam, l'objet de leur vénération est à jamais inégalable. Mais l'idolâtrie est une perversion de la fascination qui n'intéresse pas mon propos.

Que Proust (ou certaine actrice) soit idolâtré n'est pas un "effet" de Proust (ou de l'actrice), mais une production du désir qui hisse un individu au-dessus de sa propre valeur. À travers ce processus d'idéalisation, l'admirateur se sent lui-même grandi (là est la perversion) par sa prétention à dominer le champ des connaissances exclusivement dédiées à son idole.

C'est donc par la grâce qu'on lui confère, indépendamment de son domaine d'excellence, que l'artiste, le sportif, l'homme politique change de régime de valeur. L'idole n'est que dans l'œil du fidèle qui divinise, par sa faute, sa passion.

L'idole a des adorateurs (autorité divine). Le chef, des lieutenants (autorité autoritaire).

De là à croire qu'il entre quelques manières d'idole dans le jeu de distanciation du prince et des réflexes autocratiques dans sa volonté de contrôle, il n'y a qu'un pas. Au fond il n'est pas rare que le prince alimente la sidération de ses courtisans à partir d'une fonction "par ailleurs" convoitée ou depuis une réussite "par ailleurs" admirée.

Je crois donc par définition que le prince est celui qui s'attribue (ou se fait attribuer par d'autres) un "talent" qu'il possède en propre et qui le distingue des autres types de puissance engendrant des effets de fascination puisque cultiver un talent n'a jamais conféré à personne le droit d'être obéi ou révéré.

Le prince serait donc cet être chez qui l'autorité dérive du talent ou qui tire son autorité du talent, plutôt que de la force, de la fonction, de la naissance, des titres, ou de quoi que ce soit d'autre. J'espère montrer ce faisant que le "charisme" n'a pas d'essence, mais qu'il apparaît, tel un mirage, lorsque certaines circonstances très caractéristiques sont réunies.

Qu'est-ce qu'un talent ?

On sera peut-être tenté de répondre que c'est la manifestation d'une aptitude exceptionnelle révélée par un ensemble d'actions liées à un champ de compétences. Or, sans oublier qu'il existe aussi des talents nuisibles, comme dans le domaine politique ou militaire, on observe que le talent, sans usurper son nom, peut aussi bien tendre à une technicité extrême, à la virtuosité, que se présenter comme une capacité simple, voire se réduire à une maîtrise assez hasardeuse de compétences.

Relativement à leur domaine de réussite respectif, comparez le cas du musicien, du baratineur de salon et de l'entrepreneur. Le premier a un talent d'artiste (celui de livrer des exécutions remarquables), le deuxième un talent de société (celui de détourner la conversation, par exemple), enfin le dernier a le talent des affaires (celui de réaliser des gains en série).

À l'inverse, on remarque aussi que le développement d'un ensemble de compétences ou de techniques ne suffit pas à faire apparaître le talent et la reconnaissance qu'il appelle. Dit-on d'un agriculteur qu'il a du talent ? Pourquoi non ?

On préférera peut-être imaginer une ligne ascendante qui hiérarchiserait les talents du mieux réparti au plus singulier, du moins glorieux au plus exceptionnel : les talents de ménagère seraient en bas, le talent littéraire en haut. Mais alors, selon quels critères ? Établis par qui ?

Le talent a sans nul doute quelque chose à voir avec l'aptitude, la capacité, le savoir-faire, bien que de manière non exclusive. Aussi, je crois que ces termes ne devraient pas, ne serait-ce que par prudence, être traités comme de simples synonymes.

Le talent n'est pas non plus le "génie" (substantif par lequel les foules reconnaissantes ont pris l'habitude, au moins depuis Kant, de consacrer l'homme intellectuellement supérieur) : juste la lampe d'où il sortira un jour, peut-être. Une boîte qui attire les regards, mais dont le contenu reste dissimulé, et sur la valeur duquel on peut spéculer à loisir.

Ainsi, tout l'art du prince consiste à faire monter les enchères à propos de ce don énigmatique et reclus, et à persuader autrui (comme lui-même) de donner crédit à ce mystère. Plus l'enjeu est élevé, plus le prince est conforté dans l'idée de sa valeur, et plus il se prend, ainsi reconnu, pour un homme de rang supérieur, bien qu'à proprement parler il n'en soit pas (encore) un.

Enfin, le talent peut très bien être réel, avéré, confirmé. C'est-à-dire que ce talent, ayant surmonté avec succès et publiquement une épreuve de vérité, est désormais "reconnu".

À ce stade, deux carrières s'offrent au possesseur de ce talent. Soit ce talent "reconnu" peut œuvrer avec transparence en soumettant l'avenir de son aptitude à une régulation continue, soit il peut chercher à forcer son avantage au mépris de son devoir de droiture. Il abuse alors de la reconnaissance acquise en l'employant à des fins de fascination. Mais ce type d'escroc mérite davantage le nom de "gourou" que de prince. (J'y reviendrai.)

Si je vous disais posséder au fond d'une armoire une chose d'une espèce tout à fait remarquable, ne seriez-vous pas désireux de la voir (même un peu) ? Et si vous découvriez après coup que je mentais, ne me traiteriez-vous pas d'escroc ?

Le prince ou individu charismatique n'est-il pas justement ce type de personne capable de faire naître un désir de savoir tout en entretenant une incertitude quant à l'objet de ce même savoir ? Cette capacité-là ne témoigne-t-elle pas aussi d'une forme de talent, quoiqu'elle ne soit pas à proprement parler une aptitude exceptionnelle ?

# De la parade à l'assentiment : théorie des "signaux"

`\textsc{Avant de démêler}`{=tex} cette pelote de problèmes, commençons par comprendre comment naît cette alliance contre nature du faible et du puissant et tentons de circonscrire l'histoire de leur rencontre. Qui va chercher l'autre ? et pour quel motif ?

On remarque vite à la lecture de Saint-Simon que la première prise de contact, qui s'échelonnera peut-être sur plusieurs entretiens, est décisive. Ou bien rien ne se passe, ou bien, par un prodige de complémentarité, le courtisan est aussitôt allié avec le talent qu'il désire servir (ou dont il veut se parer).

En raison du magnétisme qui opère, il n'est pas excessif de dire que le rite d'introduction ressemble à une parade. Ainsi, les partenaires se jaugent en mesurant leur position respective et simulent à échelle réduite l'éventualité d'une union en s'appuyant sur les caractères secondaires de leur échange verbal : l'enjeu consiste, lors de ce premier contact, à apprendre qui mènera l'autre en laisse. Par l'émission de "signaux" qui traduisent son vouloir-plaire, le courtisan rend visible son attitude de soumission tandis que simultanément le prince décide de s'approcher de lui ou de s'en écarter.

La théorie dite des "signaux coûteux" qui définit la notion de "signal" au sens employé ici est d'abord née dans le contexte de la biologie de l'évolution avant d'être réélaborée dans une perspective philosophique par Jean-Marie Schaeffer (2009).

Difficile à étayer sur la base d'observations, non moins problématique --- intellectuellement parlant --- que l'introduction du *clinamen* dans la physique épicurienne, cette hypothèse offre néanmoins l'avantage de remonter jusqu'à la brisure de symétrie initiale autour de laquelle va s'organiser le couple prince-courtisan. Sans le recours à la théorie des signaux, l'asymétrie des rapports de pouvoirs entre le prince et sa cour ne peut être que constatée, jamais expliquée. Même en décomposant méticuleusement la séquence de leur rencontre, l'asymétrie semble préexister, comme si elle avait toujours été là.

Ainsi si l'on en croit Saint-Simon, la première rencontre est immédiatement perçue par le courtisan comme une "aubaine", alors que pour le futur prince, elle est plutôt vue ou accordée comme une "promotion". Le pli est pris à l'instant où chacun (se) reconnaît (à) sa place.

La rapidité avec laquelle les rôles sont distribués contribue de manière tout à fait primordiale à fortifier l'association qui va naître. Ayant eu lieu quasiment instantanément, l'appariement ne permet pas de comprendre que l'attribution de ces rôles résulte de l'interprétation de signaux échangés et non d'une affinité élective.

Voilà pourquoi ce prologue, dont la finalité est de mesurer le plus tôt possible si le courtisan va se soumettre et par là même occuper une place de confiance, endosse instantanément et solennellement, la valeur d'un pacte : c'est la promesse d'une alliance.

Et cela, même si la nature de cet engagement tacite ne se précisera que par la suite, au fil de leur fréquentation.

L'hypothèse des signaux suggère qu'il n'y a pas à proprement parler d'enquête linguistique à mener sur la nature des énoncés échangés entre deux partenaires qui se rencontrent pour la première fois. Ce qui se dit ou la forme des propositions employées importe moins que la perception des signaux échangés.

Ces signaux, donc, seront définis comme des gestes ou des attitudes sans fonction signalétique autre que la non-simulation. Le signal est une manifestation corporelle qui agit à un niveau *infrasémiotique*. Un signal ne communique pas. Ce n'est pas un signe. Il n'est pas traductible. Il n'est pas déchiffrable en langage ni interprétable en termes d'intention. Il est dépourvu de sens.

En aucun cas, il ne relève de la "communication non verbale". Certes, une rougeur qui enflamme les joues ou un regard qui s'abaisse signale la présence d'une émotion ; cependant il serait aberrant de croire qu'on peut attacher une signification à ce signal en dehors du fait que l'émotion manifestée n'est pas feinte et qu'elle ne peut pas l'être.

En aucun cas non plus, il ne doit être considéré comme l'élément d'une grammaire qu'il s'agirait d'assimiler. Soutenir qu'il existe, d'un côté, des "signaux robustes" (inspirant la confiance), de l'autre, des "signaux faibles" (suscitant la méfiance), c'est assimiler les échanges sociaux à un langage codé (Origgi) ; et trivialement les signaux à des signes.

On peut tenter d'agir pour contrôler de manière très étroite l'émission de ces signaux, comme l'exigeaient les codes d'interactions mondaines de la société d'Ancien Régime. Il n'empêche que la production du signal, lorsqu'elle a lieu, est la conséquence d'une propriété réelle.

Quoique ces signaux appartiennent à une gestuelle socialement incorporée dans la mesure où ils diffèrent d'une culture à une autre, d'une société à une autre, d'un groupe à un autre, ils ont une valeur de sincérité universelle : partout ils expriment la présence d'une propriété sans simulation possible.

Pour valider cette hypothèse, il faut m'accorder trois points : (1) de tels signaux existent ; (2) ces signaux n'ont aucune implication de sens ou d'intention ; (3) la sincérité qu'expriment ces signaux peut être exploitée parce qu'elle est absolument authentique.

Si ces points sont admis, je peux expliquer pourquoi le fait de baisser les yeux ne signifie pas nécessairement que vous manifestiez votre soumission, mais aussi pourquoi ce même regard baissé est susceptible d'offrir une "prise" à quelqu'un qui voudrait essayer de l'exploiter.

Bien entendu, l'émission de ces signaux est plus ou moins scrutée en fonction des milieux dans lesquels ils se produisent et elle fait l'objet de tentatives de contrôle plus ou moins conscientes, mais cet aspect est négligeable dans notre contexte. Le fait remarquable est que ces signaux sont, non pas la cause à proprement parler, mais le fondement, en tant que prise universelle, de l'asymétrie à partir de laquelle le prince va parvenir à prendre l'avantage.

Saint-Simon fait observer qu'à l'issue d'une première entrevue type le courtisan finit immanquablement par "se rendre aux charmes" du prince qui, réciproquement, assure toujours "avoir été charmé" de leur rencontre.

Remarquez combien ces deux énoncés sont symétriques sur le plan du sens : ce sont deux manières, convenues, de reconnaître qu'une sorte de charme opère.

Et cependant, leur direction d'ajustement diffère. L'un envoie un signal de capitulation ("se rendre") ; l'autre fait connaître son goût. L'un admire ; l'autre dit qu'il aime (être admiré). Voilà l'échange scellé, les rôles distribués.

L'étrange ballet qui se déploie entre signaux de soumission et de domination doit encore, pour concorder, aboutir à la "figure" de l'assentiment qui fait passer ces positions --- dominante ou soumise --- de l'attribution à l'acceptation. C'est la figure de l'assentiment, par laquelle chacun consent à tenir désormais sa place, qui a valeur d'acte constituant.

Tous les énoncés produits pendant la première prise de contact entre prince et courtisan s'appuient, même minimalement, sur des signaux qui les orientent. Bien sûr, la connaissance du rang ou de la fonction de l'interlocuteur interfère avec les signaux perçus lors de la rencontre soit pour les affaiblir soit pour les renforcer. Dans les faits, il n'y a aucune situation pure : toute rencontre est préconditionnée par un horizon d'attente et de savoir. Toutefois, comme nous ne traitons pas des cas de soumission induite par le respect ou l'obéissance dus à l'autorité, ces interférences peuvent être mises entre parenthèses.

Je soutiens l'idée que c'est l'identification, consciente ou inconsciente, d'une brisure de symétrie associée au désir, conscient ou inconscient, d'exploiter cette brisure qui autorise le puissant à s'exprimer publiquement, à faire connaître ses préférences en matière d'opinion ou de goût. Ainsi, lors de l'entretien type, le prince s'autorise à dire qu'il trouve mauvais que X en spéculant sur la base de signaux --- non intentionnels bien qu'exprimant des propriétés sincères --- que son avis s'imposerait.

La première prise de parole en face d'inconnus est fondée sur la perception, adéquate ou non, de ces signaux, étant entendu que cette perception est instantanément rectifiée, ajustée, corrigée, recalculée par les énoncés qu'elle a permis d'engager.

Au fond, l'hypothèse des signaux n'est qu'une fiction utile expliquant la hardiesse de certains individus à faire connaître leur revendication de puissance par le fait qu'ils croient pouvoir se donner un avantage sur autrui. Devant la faiblesse, l'hésitation, le doute, la probité que la gestualité d'autrui rend visible malgré lui, là où d'aucuns se sentiraient simplement émus ou indifférents, un certain type de personne s'attribuera l'idée de sa supériorité.

En l'occurrence, le prince se prête à lui-même la croyance que son opinion sera crue sur la foi d'un signal laissant à penser que, peut-être, en effet, elle le sera. Il se risque à le croire selon un calcul qui tient compte de la connaissance de sa propre valeur. Au fil de la fréquentation du prince, au cours des échanges suivants qui mobiliseront des stratégies beaucoup plus conscientes et beaucoup plus actives, ce rapport de supériorité sera renforcé par un ensemble de techniques qui tenteront de faire croire à sa naturalité.

Dès lors, la question des signaux deviendra secondaire, invisibilisée, passée sous silence (il n'en sera, de fait, plus jamais question). D'ailleurs, comment rendre leur objectivité à ces signaux sans prendre le risque d'être assimilé à ces entreprises de "décryptage" pseudo-scientifiques du codage non verbal ?

C'est pourquoi, parce que la domination des esprits est bâtie sur une rupture de symétrie astucieusement exploitée, la réflexion doit à présent changer d'échelle et se pencher sur les techniques d'exploitation permettant d'accroître cet avantage primitif.

# Évaluation, valeur, critères de valorisation

`\textsc{Il faut}`{=tex} qu'ait lieu la "figure" de l'assentiment pour que se scelle l'amitié étrange, réciproque, mais asymétrique, du prince et de ses courtisans (chacun pris un par un).

J'appelle "assentiment" l'opération par laquelle les opinions de la cour se synchronisent avec celle du prince, abstraction faite des efforts produits pour conduire à cette harmonie.

L'assentiment ne survient pas en tant qu'aboutissement d'un processus mental ou en tant que conclusion logique d'une opération longue et laborieuse, mais sur-le-champ, spontanément : le courtisan se reconnaît au miroir du prince, qui prend plaisir à se reconnaître en lui. Le jeu de reflets dans lequel se coordonnent deux désirs et deux goûts est le résultat de cette figure fondatrice ratifiant une inégalité de rang qu'elle préfère se figurer comme un consentement.

Cette parité illusoire d'une amitié scellant une communauté de "valeurs" ne sera jamais renégociée, quand bien même elle résulte d'un accord unique. On peut, du reste, se représenter la figure de l'assentiment comme une espèce de pacte faustien où ce qui a été consenti une fois, dans des circonstances opaques ou bien par égarement, vaut pour l'éternité.

Je crois en effet que dans des relations de ce type les positions restent, sans évolution possible, ce qu'elles furent originellement. Délibérément maintenu en état de minorité, le courtisan est, dès l'instant où il a consenti, privé du libre usage de son jugement. (J'y reviendrai.)

Or, "consentir" ce n'est pas exprimer oralement son accord, c'est seulement entrer dans le mode d'être du prince, tolérer ses opinions, ses goûts, ce qui revient tacitement à admettre la supériorité de son talent. Une fois la figure exécutée, qu'il prenne un temps bref ou considérable à s'initier aux manières de la cour, le courtisan doit apprendre à penser comme le prince pour espérer un jour devenir un "favori" : la plus haute dignité que peut revêtir ou dont peut rêver le courtisan. La période d'initiation qui succède à la figure de l'assentiment représente une forme d'"épreuve" au cours de laquelle le courtisan est tenu de faire la démonstration de ce qui deviendra sa qualité première : la "loyauté". En effet, seule une loyauté sans faille pourra lui valoir d'être admis dans l'amitié du prince.

Le prince et son satellite gravitent autour d'un même point fixe, chacun mû sur son orbite par un identique désir d'avancement. Là où le prince souhaite augmenter la reconnaissance due à son talent, le courtisan veut se hisser dans la hiérarchie de la cour (où il trouve aussi une forme de reconnaissance). Cette ambition commune définit le contenu de leurs priorités, et en même temps subordonne celle du courtisan à celle du prince, dont la préoccupation principale consiste dans le maintien au plus haut niveau de la cote de sa valeur. Ne pas être "diminué" dans son autorité, dit Saint-Simon. C'est à cette entreprise qui ne le concerne pas ou alors seulement de très loin que le courtisan se retrouve embrigadé, à ce projet que son ambition est détournée.

Il y a donc assentiment quand le prince entend dans la bouche du courtisan l'écho fidèle de sa propre pensée et que simultanément le courtisan place sa puissance d'opinion sous la protection du prince. Quand le courtisan se rallie aux avis du prince et qu'en contrepartie le prince le consacre dans ce rôle. Quand le courtisan lui fait l'offrande de son suffrage et qu'en contrepartie il reçoit des faveurs qui protègent. De part et d'autre, par un acte libre et volontaire, on consent. C'est une co-acceptation. Ce consentement inaugural, lorsqu'il advient, dessine la figure de l'assentiment. Par elle, et seulement par elle, le courtisan entre dans les bonnes grâces du prince.

L'échange de loyauté qui s'opère avec l'assentiment repose initialement sur la base des signaux échangés et de leur évaluation, indépendamment des énoncés proférés dans la même circonstance.

Par là, je voudrais insister sur le fait que la cooptation s'ébauche dans une complicité silencieuse, qu'elle prend racine dans une entente tacite. Un regard évasif, un front incliné suffit. Du reste, la figure de l'assentiment est immédiate. Les rencontres peuvent s'échelonner dans le temps, la confiance se gagner, les habitudes se prendre, cependant l'assentiment a lieu ou non : il ne connaît pas d'états intermédiaires, il est inconditionnel (une idée déjà défendue par J. H. Newman).

En ce sens, l'assentiment est indifférent aux raisons et aux justifications. Cette particularité est étrange si on la relie à la croyance selon laquelle la loyauté est une valeur qui tend à croître et se fortifier dans la durée. Pourtant, à l'évidence, chaque fois qu'un groupe s'agrège, il y a en son sein des alliances de fortune qui se créent, même si ce groupe possède une durée de vie limitée (réunion) ou qu'il est formé artificiellement (soirée).

Ces ralliements de circonstances représentent un cas particulier de notre description générale en ce sens que les loyautés échangées ne le sont que temporairement et contextuellement. C'est un pacte à durée déterminée inscrit dans la durée elle-même programmée du groupe où prend effet ce pacte. Logiquement, lors de circonstances provisoires, on forme des liaisons provisoires.

Le vrai fait du prince tient tout entier dans cet exploit : convaincre que X est mauvais en disant "je trouve mauvais que X...". La question n'est pas de savoir si c'est un sophisme, puisque c'en est un, mais à quelles conditions ce sophisme agit, devenant capable de modifier l'opinion d'autrui.

Nous pouvons désormais offrir une réponse (au moins provisoire) à ce problème. Pour qu'un prince parvienne à imposer son goût, il faut non seulement qu'il soit convaincu de son talent, qu'il sache ce qu'il vaut, mais qu'il rende crédible l'existence de ce talent qu'il croit --- à tort ou à raison --- posséder de sorte que son auditoire puisse spéculer à loisir sur son étendue. Mais cela n'est pas encore assez. Il faut aussi que le prince sache identifier à quelques signaux exploitables, ayant le désir de les exploiter, qui parmi son auditoire consentira à consacrer la supériorité de son talent en échange d'une quelconque perspective d'avancement. Il faut finalement qu'il donne son assentiment à la tenue de cet échange.

Deux choses me paraissent cruciales et tout à fait significatives à ce stade.

La première, c'est que durant toute cette phase d'appariement les réflexes rationnels sont neutralisés, chacun dans le groupe déploie des stratégies de survie, des stratégies fondées sur des instincts sociaux biographiquement ancrés. Il n'y a pas, à ce stade d'ébauche, de la part du prince, de calcul machiavélique conscient, mais seulement l'activation, comme chez les autres membres du groupe, d'un système archaïque de détection de menaces ou d'alliances potentielles.

La seconde, c'est qu'une fois exécutée, la figure de l'assentiment doit permettre d'imposer des goûts sans rencontrer de véritable opposition.

J'insiste sur le fait qu'un jugement de goût ne doit pas être confondu avec les valeurs qui sous-tendent ce jugement et qui correspondent, ici, aux justifications de ce goût.

Certes, l'affirmation d'un goût positionne celui qui prend position en l'énonçant, elle le "distingue", mais, contrairement aux valeurs qui peuvent faire l'objet d'une montée en objectivité, les goûts, bien qu'exemplifiant des valeurs, ne se discutent pas, du moins pas à un tel niveau de généralité.

Un goût ("trouver mauvais") éclaire partiellement ou fait entrevoir l'univers de valeurs de celui qui l'énonce, son récit du monde, tout en se gardant bien d'y donner accès. Les goûts sont proclamés avec ostentation et débattus avec passion. Les valeurs que ces goûts exemplifient sont rejetées à l'arrière-plan et les critères de valorisation (qui fixent la valeur de la valeur, qui justifient que le fait que telle valeur soit valorisée) sont maintenus dans l'ombre, volontairement dissimulés. Les jugements de goût, dans la bouche du prince, valent donc pour l'expression d'un attachement subjectif faisant écran à une possible montée en objectivité. Ils expriment une opinion issue de l'expérience de l'énonciateur en même temps qu'ils évitent de faire connaître la chaîne des raisons par laquelle cette expérience trouve sa valeur de vérité. D'une part, ce goût, porté par une conviction, n'est pas réfutable ; d'autre part, ainsi cachés, les critères de valorisation de ce goût peuvent changer en fonction du but poursuivi, selon une géométrie variable.

Le vice épistémologique qui se masque derrière cette attitude consiste à se servir de préférences personnelles pour imposer un récit du monde où dominent un certain type de valeurs afin de ne pas avoir à exposer les critères sur lesquels s'appuie la promotion de ces valeurs en particulier (et non d'autres).

L'intérêt de cette attitude est double : il permet à la fois de taire l'éventuelle friabilité de ces critères et de modifier ces critères de manière opportune en fonction des buts à atteindre. Car aussi longtemps qu'une valeur n'est pas rapportée aux critères qui la dotent d'intelligibilité, elle reste indéterminée quant à son sens. Une même valeur (par exemple, l'individualisme) peut prendre un sens positif ou négatif suivant les critères de valorisation sélectionnés ; de sorte que toute valeur (ou presque) peut devenir une anti-valeur (Heinich).

Nous disons ainsi que la signification d'une valeur ne peut se déterminer que sur un fond de savoir qui ne fait pas partie lui-même du sens de la valeur, et que la dissimulation de ce savoir profite très largement au prince. De même qu'un texte codé se comprend à l'aide d'une clé de détermination, un récit du monde où cohabitent de multiples valeurs se déchiffre à l'aide de critères de valorisation.

Illustrons concrètement cet écart.

Chacun sait que la force de dissuasion policière s'incarne dans le port de l'uniforme et de ses insignes, et au-delà dans les critères auxquels la valeur d'ordre public renvoie. Dans cet exemple, la valeur positive ou négative d'ordre public est figurée par des objets (pour rendre la logique plus sensible). Dans la situation qui nous occupe, une valeur l'est par le truchement d'un jugement.

Les critères auxquels renvoient ici les insignes policiers forment la matrice, virtuelle, mais actualisable en cas de litige, de tous les arguments légitimant la délégation du pouvoir aux forces de l'ordre. Qui ignore les critères ignore le soubassement --- la "raison d'être" --- de la valeur.

Ainsi, comme un objet symbolique, l'expression d'un jugement de goût possède des qualités pratiques, en ce qu'il permet de ne pas avoir sempiternellement à faire la somme de toutes les raisons justifiant une conduite. Naturellement, il en va autrement quand, comme dans le cas où on cherche à obtenir de la soumission par l'expression de préférences, ces critères de valorisation sont délibérément maintenus à l'état implicite, sans possibilité de les actualiser ou d'y référer, même en cas de désaccord.

Il faut donc qu'existent des critères de valorisation formant le soubassement des valeurs manifestées au travers de l'expression d'un jugement de goût. Soit dit en passant, l'existence de tels critères ne suppose pas qu'un débat rationnel puisse, de quelque manière que ce soit, mettre un terme aux conflits d'interprétation au sujet du sens de ces valeurs ; tout au plus permettrait-il de clarifier les termes d'un désaccord sans prétendre le résoudre. Il ne suffit toutefois pas de refuser l'explicitation de cet arrière-plan pour obtenir des manifestations de soumission de la part d'un courtisan. Il faut encore remplir deux conditions.

D'abord, les valeurs doivent se combiner en système, c'est-à-dire que la correction ou validité des gestes, des attitudes et des jugements doivent être rapportables à des normes convenues, comme c'est le cas des règles de politesse, de l'étiquette, des protocoles, etc.

Ensuite la maîtrise de ce système doit être évaluée sur des bases non explicites, autrement dit selon des critères délibérément opaques ou fluctuants. Pensez ici à tous ces groupes dans lesquels le prestige symbolique s'est substitué à la qualité effective comme monnaie d'échange et où l'intériorisation de ce troc vaut pour règle de sélection. Il y a obligation de ne pas mettre au jour ce fonctionnement pour intégrer le groupe ou s'y maintenir. Plongé dans le chaudron, le profane ressent vite qu'il ne possède pas les bons codes (et c'est généralement ce qu'il dit). Mais il ne comprend pas que ce n'est pas le code qui lui fait défaut, mais les critères d'arrière-plan qui font tourner ce code.

Je résume. Au fondement de l'asymétrie prince-courtisan, il y a des signaux qui assignent des positions (dominant-dominé). Puis, au sein de ces positions, l'expression de préférences personnelles qui manifestent des valeurs (individuelles ou de groupe), elles-mêmes adossées à des critères de valorisation. Le prince abuse de sa position dominante en imposant ses valeurs sans les justifier par des critères. Le courtisan, en acquiesçant à l'expression de ces préférences, épouse ces valeurs sans (pouvoir) les questionner. Grâce à la figure de l'assentiment qui institue entre les deux des devoirs de loyauté, cette liaison devient stable et dure dans le temps.

# Éblouir les sots

`\textsc{Pour qui l'observe}`{=tex} du dehors, la conduite du prince est hermétique. Reste à jamais mystérieuse la manière dont il impose ses avis, son autorité sur les esprits.

Montaigne parlait du fondement "mystique" de l'autorité, Montesquieu de "prodige" ou encore de "magie". De quel autre nom ces auteurs auraient-ils pu affubler ce tour de passe-passe, puisque l'exercice de la fascination, le pouvoir de persuader les esprits sans production d'arguments, le charisme enfin, repose toujours sur une double dissimulation ? Or, qu'est-ce qui est dissimulé ?

D'une part, le talent lui-même, celui qui, en droit, fonde la légitimité du prince : la valeur de son talent n'est jamais montrée, encore moins discutée. Le prince doit tenir son rang (rang strictement symbolique qui découle de son talent puisqu'il n'exerce pas de pouvoir tangible), mais le talent qui fonde ce rang n'est jamais entièrement connu. Si être pourvu d'un talent vous range (aux yeux de ceux qui reconnaissent son existence) au-dessus de ceux qui s'en trouvent dépourvus, alors le talent vous élève (un peu) même si son existence n'est pas démontrée.

D'autre part, l'arrière-plan sur le fond duquel s'exerce ce talent est dissimulé : ses critères de valorisation sont insaisissables. Ce deuxième aspect explique les si fréquentes pirouettes du prince, appelées "caprices" dans son entourage, par euphémisme. Car en vérité, les volte-face, les contradictions, les rétractations du prince sont d'indispensables manœuvres d'enfumage destinées à entretenir la confusion sur le soubassement des valeurs qui composent son récit du monde. Par conséquent, à soustraire ses avis du débat public.

Si le talent est la valeur sur laquelle on spécule, alors le rang représente le savoir de cette valeur (comme dans l'expression "il sait ce qu'il vaut"). En d'autres termes, c'est la conscience de la valeur en tant que celle-ci se fait connaître publiquement. On dira pareillement que le rang est la face externe du talent : il se mesure, s'apprécie, se soutient, se convoite. Il correspond ni plus ni moins à la manière dont le prince souhaite être vu, souhait qui coïncide, dans le meilleur des cas, avec l'image qu'il a de lui-même.

Tout se passe donc comme si, à travers les opinions qu'il exprimait, le prince cherchait à imposer l'opinion qu'il avait de lui-même. Par ses avis, il ne prétend pas seulement convaincre, mais "briller". Être connu par son talent. D'abord auprès de sa cour, sa meilleure compagnie, puisqu'il sait d'avance qu'elle lui donnera son approbation, puis, ambitionne-t-il, au-delà. Dans la mesure où celui-ci tend rationnellement à maximiser la valeur de son talent, tout prince cherche à accroître son rang en élargissant son audience. Or, et c'est ce qui importe ici, l'extension de son domaine d'influence passe par le relais des courtisans qui jouent invariablement et malgré eux ce rôle d'amplificateur.

Je propose d'appeler "éclat" cette manifestation idéale de soi fondée sur le savoir de sa valeur. L'éclat est une marque d'orgueil en même temps qu'une revendication d'autorité pour faire admettre la prééminence de sa place. Ce dernier aspect autoritaire rappelle que l'éclat n'est pas une donnée objectivable, ni pleinement observable. Cette sorte d'étincelant peut ou non apparaître.

Il a d'ailleurs plus de chance d'apparaître entre des gens qui partagent les mêmes préoccupations. Hors d'un domaine de compétence ou d'un cercle d'intérêts, il devient difficile d'estimer la valeur d'un quelconque talent. Celui qui, parce qu'il ne le voit pas, reste indifférent à l'éclat du prince affichera à son égard une décontraction et un détachement souverains en toutes circonstances. En revanche, sitôt sa présence détectée, le même qui ne voyait rien un instant auparavant manifestera au prince le juste degré de déférence qu'il croit devoir à la reconnaissance de son rang. Vous pouvez ainsi vous montrer insensible à l'aura d'un chirurgien tout en admirant un professeur de littérature, et vice-versa.

La considération qu'engendre l'éclat est si facile à obtenir que le prince aurait tort de ne pas mettre toute son industrie, soit une part non négligeable de son talent, à créer de lui-même cette image étincelante. Chez le prince en effet, le talent sert aussi, et peut-être avant tout, à produire la brillance censée manifester la valeur dudit talent. Car le talent lui-même, par définition soustrait à la confrontation publique, n'est que sporadiquement dévoilé. Aussi ce talent, pour se faire connaître, doit-il emprunter des voies indirectes et se soumettre aux moyens de la représentation.

C'est toute la fonction de l'éclat que de mettre en scène le talent dans sa forme idéale, de représenter le prince en majesté, fier de savoir ce qu'il vaut et désireux de le faire savoir. Après quoi, comme n'importe quel arcane, l'éclat n'est plus guère interrogé par ceux qui l'aperçoivent, encore moins critiqué, ce qui permet au prince de prendre l'ascendant sans lutte. Voilà pourquoi l'intérêt premier du prince n'est pas tant de perfectionner son talent (même si celui-ci doit être entretenu régulièrement pour ne pas se dévaluer) que d'"éblouir les sots", selon l'expression de Saint-Simon, afin d'étendre son bassin d'influence.

Si le public du prince est souvent fourni de sots, c'est peut-être que le "sot" n'a rien d'un imbécile. Assurément le sot n'est pas un individu court de savoir ou de peu d'esprit, celui qui ignorant tout veut être pénétrant, mais plutôt quelqu'un qui renonce "aux raisons appropriées de croire" (Engel).

En tournant le dos, au moins temporairement, à l'exigence de sincérité qui fonde toute interaction à visée communicante, le sot est le type même de celui qui accepte de se soumettre momentanément au jeu du prince, à son système de signes et d'opinions, pour le seul plaisir de voir où ça mène, tout en conservant l'illusion de ne pas en être la dupe. Tout cela avec un mélange de fausse candeur, de paresse intellectuelle, de curiosité distante, de désinvolture. Il veut voir, se laisser convaincre, se donner le temps d'étudier s'il ne serait pas plus profitable de retourner son avis. Éprouver la robustesse de celui du prince. Et pour cela, il consent sottement à mettre entre parenthèses son jugement.

Le sot n'est pas encore un courtisan puisqu'il n'a pas de commerce privé avec le prince, mais par la suite peut-être le deviendra-t-il, car c'est logiquement parmi les sots que le prince va chercher à recruter les futurs élus de sa cour ou à gagner ceux dont il a besoin.

Parce qu'il a renoncé à toute prétention d'exactitude, parce qu'il s'est abandonné aux manœuvres du prince (même si par-devers lui il croit rester critique), le sot, devenu maniable, n'a en réalité que deux possibilités d'action, et deux seulement. Soit il quitte le jeu par désintérêt et retrouve son indépendance de jugement, soit il tombe dans le giron du prince, succombe à sa fascination avec le désir de déclencher la figure de l'assentiment dans l'espoir d'intégrer sa cour.

Ce qui pourrait se traduire en disant que les interactions du sot avec le prince sont limitées à cette polarité : désintérêt ou admiration. Or cela --- l'absence de mode d'interaction intermédiaire --- représente une aubaine pour le prince, et fait du sot la recrue idéale. Désintéressé, le sot vaut une perte nulle ; admiratif, un gain total.

Il est crucial pour le prince d'apprendre à distinguer rapidement et sans effort, comme naturellement, les sots dans le troupeau de ses interlocuteurs.

En réalité, l'habileté que cela requiert est minime, car les occasions se présentent d'elles-mêmes. Le sot qui paraît en public a pour autre caractéristique (quand il n'est pas simplement un peu badaud) de se signaler comme une personne "sans monde", c'est-à-dire n'ayant pas la maîtrise des normes régissant les interactions du milieu où il se trouve. Maladroit dans l'usage de la civilité, fondu dans la masse ou au contraire tenu à l'écart, incapable de se faire entendre, peu sûr de lui-même, comptant pour rien, il cherche une protection qui l'introduira dans le groupe pour lui permettre de prendre part à la conversation tout en se taisant (pour éviter d'avoir à faire l'aveu de son incapacité).

Il est également dépourvu du talent de briller. Ce qui ne signifie pas, encore une fois, qu'il soit privé d'aptitude ou d'intelligence ; au contraire, le sot partage très souvent le même intérêt pour le domaine où s'illustre le talent du prince (sans quoi ils ne se retrouveraient pas en présence). Seulement, soit il en possède une maîtrise inférieure soit il ne sait pas (ne veut pas) mettre son talent en scène, soit encore les deux à la fois.

On peut dire que le prince est bien aidé par le sot au moment où il cherche à conquérir ou à élargir son audience. Celui-ci envoie lui-même le signal de sa sottise en se montrant inapte à interagir en groupe et à valoriser publiquement son aptitude.

Le portrait du sot et, par extension, celui du courtisan sont complétés par cette nouvelle observation (rappelons que tous les courtisans sont des sots, mais que tous les sots ne deviennent pas courtisans).

Si dans une interaction de groupe le sot consent à mettre provisoirement son jugement entre parenthèses, c'est qu'il ne sait pas mettre en scène son talent. Il apparaît dépourvu de talent aux yeux du groupe, alors même qu'il cultive généralement une aptitude ou un savoir-faire identique à celui du prince, ou en lien avec son domaine de prédilection. Sauf que l'aptitude du sot n'accède pas à la représentation et passe pour une carence ou une déficience.

On dira en ce sens qu'un "savoir-faire" est un talent non représenté (qui n'accède à aucune forme de reconnaissance publique). Alors que le talent, lui, est la mise en scène d'une aptitude jamais manifestée.

Pour envoûter le sot, le prince possède l'avantage de son talent qui rayonne au-devant de lui. On a déjà fait valoir précédemment que cet éclat émanait de la revendication du rang dû à son talent, mais qu'il était faux de prendre cet éclat pour la manifestation directe de son talent (pour laquelle il prétend passer néanmoins).

Il y a ici un flottement sur lequel le prince joue habilement. En réalité, c'est un abus.

Dans le contexte grégaire des effets de fascination qui nous occupe --- mais peut-être aussi plus universellement ---, on accorde un crédit d'autorité à une personne sur la foi de son talent. Avant de pouvoir l'attribuer à son porteur, il faut commencer par croire à l'existence d'un talent. Or l'équivoque dont le prince abuse se loge dans cette prémisse.

Nul n'ignore que pour croire à l'existence d'une chose, il faut de cette existence une preuve ou un témoignage. Ainsi, celui qui est capable de manifester son talent à la demande, acceptant de le soumettre régulièrement aux épreuves de validité, administre la preuve de l'existence vérifiée de son talent : il possède un talent "qualifié" et jouit d'une autorité légitime. En revanche, celui qui vit sur la réputation de son talent, qui capitalise son estime de lui-même sur la manifestation unique d'un coup d'éclat passé (qui ne survit d'ailleurs ou ne se relaie que dans le témoignage) se contente de laisser dire que son talent existe : celui-là possède un talent "déclaré" et jouit abusivement de son autorité, dévoyant celle-ci en l'employant comme moyen de séduction.

On objectera qu'il n'est pas rare de voir dans la réalité le représentant d'une autorité légitime engendrer des phénomènes de soumission, même si ce dernier n'a pas cherché explicitement à les susciter. Celui qui est capable de manifester à volonté son talent devrait, en raison même de sa qualification, induire une juste considération et du respect de la part de ceux qui en sont les témoins, qui plus est, s'ils appartiennent au même domaine de spécialisation et qu'ils sont, par conséquent, des témoins éclairés.

Le respect est une attitude appropriée qui invite à mesurer avec objectivité la différence de degré de maîtrise dans l'exercice d'une même aptitude. La soumission qui vient se greffer ici, si elle a lieu, n'est qu'un effet parasite nourri typiquement par le besoin de reconnaissance sociale ou la poursuite d'intérêts privés (étudiants, assistants, corps intermédiaire, etc.). Ce genre de soumission fonctionne comme une croyance irrationnelle en la transmission de la valeur par contiguïté (transfert métonymique de la valeur ou "effet relique").

Vous êtes ainsi amené à penser qu'être (vu) en compagnie de X transfère en vous une part de sa valeur. Ou qu'être publié dans la même revue que Y vous hisse un peu vers Y.

On observe que par une sorte de perversion de son intérêt initial, le disciple, qui se mue alors en adorateur, préfère entériner des rapports d'idolâtrie avec son maître plutôt que de tisser des liens de transmission ou d'apprentissage. Il est vrai que, loin de s'en détourner, certains maîtres se rendent volontiers coupables de complaisance vis-à-vis de cette forme pernicieuse de flatterie. Comme d'autres, ils veulent jouir impunément du pouvoir.

Dans ce cas-là, plutôt que de parler de "prince", il serait plus juste d'appeler ces maîtres qui possèdent une autorité légitime, mais qui ne décourage pas les pratiques de flagornerie, des "gourous" (Sperber).

# Bâtir sa légende

`\textsc{Revenons maintenant}`{=tex} à la question du témoignage qui joue un rôle décisif dans la constitution de la réputation en commençant par rappeler qu'un prince sans cour est sans exemple. Par définition, il n'existe pas de prince solitaire. Ce qui veut dire corollairement qu'un solitaire talentueux n'est pas un prince. À tel point d'ailleurs qu'on serait parfois tenté de croire que c'est la cour qui donne son assiette au prince ou que c'est le courtisan, en tirant de son protecteur les moyens de servir ses intérêts, qui fait du prince sa créature.

Nous détaillerons plus loin de quoi retourne précisément l'intérêt des deux camps. Pour l'heure, gardons-nous de tout excès de langage qui renverserait l'ordre des causes. Comme expliqué plus haut, l'appareillage du prince et de sa cour doit être regardé comme le résultat d'une cooptation silencieuse, plus exactement d'un ajustement bidirectionnel découlant d'une figure rare aux conséquences puissantes, l'assentiment simultané de deux individus libres consentant implicitement à s'aliéner dans leur rôle respectif au profit de l'association d'intérêts ainsi créée. Étant entendu qu'à l'intérieur de cette association les intérêts, quoique complémentaires, divergent radicalement.

Il faut compter avec le fait que le prince a commis --- ou, à la rigueur, est capable de faire croire qu'il le pourrait --- quelques coups d'éclat. Cela se déduit du fait qu'un prince sans talent ne peut tout simplement pas espérer rayonner, quel que soit, toutes choses égales par ailleurs, le degré de conscience de sa valeur.

On dira alors qu'un "coup d'éclat" est une réussite dans le domaine ou le territoire revendiqué par le prince due à la chance autant qu'au talent. Une réussite ayant notablement franchi avec succès une épreuve de validation, mais restée sans lendemain. Révélation unique et chanceuse du talent dont la remémoration bâtit la réputation. La composante volontaire de cette action lui ayant échappé, le prince ne peut s'imputer entièrement sa réussite. Le coup d'éclat, qui possède un écho limité dans le temps, exprime sa principale caractéristique dans le fait de n'être pas reproductible à volonté puisqu'il n'est pas le produit exclusif de son auteur, mais seulement une combinaison heureuse de circonstances favorables et de savoir-faire.

Sachant avec lucidité qu'il sera dans l'incapacité de rééditer ce coup d'éclat, son seul fait d'armes, ou qu'il serait trop aventureux pour lui d'y risquer, le prince va donc tenter de capter le bénéfice de cette action en la reversant au compte de sa légende.

La réédition à la demande de l'exploit ayant valu au prince admiration et respect signalerait la qualification de son savoir-faire, donc de son talent. Ce qui est antinomique.

En effet, la reproductibilité du coup d'éclat offrirait, de fait, la garantie que le prince évolue dans un système d'objectivation où chaque élément résultant de l'aptitude revendiquée peut être soumis tant à la vérification qu'à la critique. Car oui, s'il était capable de le livrer aux normes de la transparence, le prince qualifierait son talent et donnerait en même temps les gages attendus de sa valeur. Ce qui est incompatible avec sa nature (ou alors ce n'est plus un prince).

Refaire la démonstration de son talent (brièvement entrevu) n'est pas en son pouvoir, et il le sait, et il ne veut pas l'admettre, et encore moins que ce soit admis par d'autres. C'est bien parce qu'il est incapable de maîtriser totalement les conditions de reproduction de sa réussite que le prince va travailler avec tant d'obstination à en fortifier le souvenir. Sa quête pour la reconnaissance de son talent va se confondre avec la construction de sa légende.

Est légendaire toute action passée qui ne survit que sur la base de sa réputation.

Or la réputation du prince ne survit qu'en faisant ou en laissant croire par des attitudes, des discours, des prises de position que son coup d'éclat peut être répliqué. Ce qui est faux, fondamentalement.

Prenez un auteur ayant connu le succès d'une publication. Naturellement dans un premier temps ce coup d'éclat lui vaudra l'attention d'un petit cercle. Mais supposez à présent que les écrits ultérieurs de cet auteur, publiés ou non, se révèlent indignes d'une telle réputation, ne devrait-on pas logiquement révoquer celle-ci en doute ? Par quel "mystère" notre auteur parvient-il encore à imposer ses prétentions et à le faire contre toute évidence ? Il faudrait en toute rigueur parler de lui au passé, puis convenir honnêtement, après examen, qu'il a connu une réussite, mais que c'était tout.

Comme tout un chacun, vous tenez sur le monde un récit intérieur, plus ou moins épais, qui est son interprétation en termes de valeurs et de croyances. Quand même ce serait possible (à supposer qu'il soit totalement explicite à vous-même), il n'est pas utile de rendre public ce récit : il vous suffit d'en mobiliser les articles au gré de vos besoins, quand il faut justifier un choix ou défendre une opinion.

Ce récit, qui devrait en théorie tendre à la cohérence, n'est en réalité ni cohérent, ni complet, ni stabilisé. Il est par essence révisable, c'est-à-dire modifiable par des raisons de croire.

Prenons un autre exemple : vous jugez un film sans qualité (ou n'importe quel autre objet ou acte digne d'appréciation) parce que dans le monde tel que vous le mettez en récit la valeur cinématographique n'est pas exemplifiée par ce genre de film.

Mais voilà, le prince atteste, lui, au contraire, de sa qualité : il dit que cette valeur y est.

Une fois déclaré, votre désaccord exige, pour se résorber en partie au moins, l'explicitation de l'article concerné par vos croyances respectives, s'il le faut en étayant celles-ci par l'expertise de spécialistes associés à ce domaine.

Mais le point sera-t-il traité avec objectivité (dans un objectif de vérification) ? Examinera-t-on les conditions qu'il faut remplir pour pouvoir dire que cette valeur y est vraiment ?

Dans le cas où vous réviseriez votre jugement sans débattre sérieusement des critères de valorisation de cette valeur, on dit habituellement que vous appréciez X (le film) parce que P (le prince) évalue positivement X.

En vérité, ce qui se produit ressemble plutôt à ce qui suit. Votre jugement de goût est disqualifié par le jugement de P parce qu'il tient sur le monde un récit concurrent au vôtre dans lequel la valeur de X est supérieure. Vous ne retournez pas votre veste parce que vous appréciez X de manière inappropriée à la suite du constat de la perspicacité de P, mais parce que vous reconnaissez la supériorité de la valeur exemplifiée par le jugement de P sur la foi du récit qui la soutient. D'une certaine manière, vous pliez devant une valeur "virtuelle" qui, abstraction faite du récit dans lequel elle a été mise en scène, n'a pas été instanciée.

Par le bruit qu'il produit, par l'enflure de son propos, par le tranchant de ses avis, c'est-à-dire par des effets de stylisation typiquement narratifs, le récit du prince met "en valeur" la valeur sans être pourtant disposé à faire apparaître les critères de valorisation qui, seuls, doteraient vraiment cette dernière de valeur.

Le fait est que les échanges qui ont lieu dans le cadre de la relation prince-courtisan ne répondent pas aux exigences d'une "éthique de la connaissance" (Engel). En effet, le but de tels échanges n'est ni de faire apparaître la vérité ni de rechercher la connaissance dans le respect des normes de l'enquête, mais d'asseoir sa prééminence, y compris, s'il le faut, en soutenant des évidences contre-intuitives ou en slalomant entre les obstacles épistémiques dressés devant lui. Le règlement des désaccords a donc lieu dans un cadre vicié où le prince part vainqueur, quelle que soit la qualité des preuves qu'on lui oppose. La contradiction est digérée par la capacité du prince à imposer sa hauteur de vue ; ce que nous appellerons désormais, ironiquement toujours, sa "perspicacité".

En clair, le prince finit toujours par se montrer le plus perspicace (même si pour cela il doit contredire l'évidence). D'ailleurs, la sagacité de ce dernier sera d'autant plus grande qu'il maîtrisera les stratagèmes éristiques exposés par Schopenhauer dans son *Art d'avoir toujours raison*, stratagèmes permettant de se dérober sans effort aux exigences de la logique.

(Remarquons entre parenthèses que le prince, très curieusement, suit des procédures régulières quand il se retrouve à devoir argumenter face à un talent d'ordre supérieur. Son vice est d'une sorte particulière : il s'exprime seulement à l'intérieur de sa zone d'influence, à savoir sa cour.)

La perspicacité fait bon ménage avec une forme de radicalité, d'outrance verbale qui exacerbe les polarités.

Ainsi le prince se plaît-il souvent à fustiger l'ordre établi, mais uniquement pour se donner raison à lui-même. Il ne critique pour elles-mêmes ni la société, ni les institutions, ni la bêtise. Les structures sociales, le mépris académique, l'analphabétisme des foules, tout cela se confond dans ses diatribes en une cabale ourdie contre son ambition. Il n'est d'ailleurs pas nécessaire de s'engager bien loin pour trouver l'obstacle qui empêche le prince d'exercer son talent. Tout, à l'en croire, sauf lui-même.

Cette rhétorique, qui n'aime rien tant que se payer d'audace à condition de rester sagement sans conséquence, permet néanmoins au prince d'adopter une posture martiale et de poser, à son avantage, en visionnaire inspiré. En se drapant de pureté, en se plaçant sous l'égide de la justice, en se hissant au firmament pour juger, il peut à son aise débusquer tous les prudents, tous les sceptiques, les consciencieux, les raisonneurs, les objecteurs, tous les dissidents, et les chasser de sa cour exigeant sans nuance qu'on soit avec lui ou contre lui.

Enfin cette intransigeance clairvoyante rabaisse les tièdes et les convaincus qui demeurent avec lui. L'hyperbole, l'excès, la témérité et les vexations que manie le prince induisent chez le courtisan un sentiment d'infériorité propice à allumer une étincelle d'admiration. Le sot admire en effet généralement ce qu'il ne se croit pas capable de faire.

Une délibération respectueuse de l'éthique devrait, en droit, mettre tous les participants à parité, sur pied d'égalité. Convaincre par des justifications rationnelles exige de tenir l'autre pour son semblable.

Dans les faits, cette exigence est intenable. C'est encore plus vrai dans le cas que nous examinons. En consentant par assentiment à s'apparier au prince, le courtisan consent par la même occasion à donner raison aux raisons du prince. Les motivations d'une telle humilité intellectuelle seront examinées plus loin. Pour l'heure, notons seulement que le courtisan n'est pas libre en pratique de ne pas croire le prince. Cela, quelle que soit la vigueur de l'opposition qu'il manifeste au cours de leurs désaccords.

On dénoncera peut-être la circularité d'un tel raisonnement : le prince impose ses raisons de croire parce que le courtisan, n'étant pas libre de refuser, consent à croire à la supériorité des raisonnements du prince.

En vérité, ce raisonnement ne l'est pas, même si, en effet, le risque de produire des raisonnements autoréférentiels est considérable lorsqu'on évoque les fondements du charisme. Mais notre analyse récuse pour au moins deux raisons la thèse selon laquelle l'autorité du prince s'établit en vertu de sa seule "force instituante" (Revault d'Allonnes).

Premièrement, il n'est pas vrai que le prince a autorité par des discours et des attitudes faisant autorité. La légitimité de sa valeur ("avoir autorité") ne découle pas de la preuve qu'il donne de sa légitimité ("faire autorité"). Bien au contraire, le talent (d'ordre second) du prince consiste justement à ne pas donner la preuve de son talent (d'ordre premier). Il joue sur un emboîtement des ordres de son talent, emboîtement dont la nature est propice à nourrir les hypothèses les plus extravagantes sur la valeur de sa valeur.

Deuxièmement, l'ajustement qui s'opère quand prince et courtisan s'apparient, en plus d'être une cooptation, consentie donc de manière bilatérale, est une condition nécessaire, mais non suffisante de l'emprise idéologique du prince sur ses courtisans. Pour fortifier cette emprise, le prince va s'appuyer, en plus de l'assentiment donné, sur deux autres conditions. De telles conditions (examinées ci-après) réunissent un ensemble très hétérogène de techniques qui tendent, chacune par son moyen, à donner un avantage décisif, soit une position supérieure ou une épaisseur accrue, à celui qui les manie.

Examinons une situation familière : vous assistez, vous et le prince, au concert d'un musicien que vous estimez. Le concert est d'excellente facture, mais la salle est vide (à l'exception de vous). À l'issue du spectacle, vous portez le jugement que le concert était "raté". En disant cela, vous voulez sans doute exprimer qu'un concert réussi implique intrinsèquement une salle remplie, mais peut-être aussi qu'il est navrant de voir un grand artiste ne pas obtenir de la reconnaissance qu'il mérite. Mais voilà, le prince, lui, juge au contraire ce concert "réussi". Il avance l'idée que le musicien a pris du plaisir à jouer et il étaye cette idée en observant que ce dernier s'est livré sans retenue comme si la salle était remplie. Disant cela, il s'exprime contre la "doxa", soutenant contre-intuitivement qu'un concert réussi se définit par l'implication de l'artiste dans son jeu, mais peut-être aussi qu'un artiste, surtout s'il est grand, peut se passer de la reconnaissance du public.

La difficulté n'est pas qu'un même objet cristallise deux évaluations polarisées ou qu'une valeur soit réversible selon les croyances auxquelles on adhère. Non, la difficulté essentielle est de savoir pourquoi, à jeu égal, dans un contexte homogène ou supposé tel, une évaluation l'emporte sur l'autre.

La validité et la force des arguments en présence ne sont pas discutées convenablement, alors qu'ils pourraient l'être, sans préjuger de la conclusion, et néanmoins à la fin du débat, dans l'entourage du prince, on se souviendra (collectivement) de ce concert comme d'un concert "réussi". Le prince aura fait quelque chose à l'objet évalué en lui attribuant une certaine valeur par le biais d'un jugement. C'est un cas où la perspicacité du prince aura infléchi le récit du monde.

Il est clair qu'une telle mésentente est possible non seulement avec des jugements, mais aussi avec des assertions pseudo-logiques comme celle-ci : "L'injonction à être fort produit la faiblesse par quoi la société se maintient".

Le désaccord porte alors sur un contenu épistémique. Il importe donc de comprendre comment, en l'absence de tout examen des raisons, le prince parvient à produire une distinction (entendez, une assertion ou un jugement qui se "distingue" de l'idée type en la matière, du *quod decet*, du "comme il faut penser"), distinction qui lui rapporte un surcroît de considération.

Nous avons déjà identifié une première condition de facilitation, c'est la figure de l'assentiment qui place d'emblée le prince au-dessus de la contradiction, si âpre soit-elle, puisqu'il entre, on l'a vu, dans la nature du courtisan de ne pas oser donner aucun signe de dissension (il peut aller à l'encontre, mais non diverger ou inverser le cours d'une interprétation).

La deuxième condition réside dans la capacité du prince à imposer ses vues sans se soumettre à l'épreuve de l'opposition. Cette capacité, la perspicacité, s'exprime à travers tous les écarts-types de son discours qui, en vertu de la première condition, échappent à l'examen. La singularité des idées qu'il soutient dissimule en réalité le fait que le prince sait d'avance (première condition) être en mesure de les faire valoir ; dissimulation qui laisse croire à de l'audace là où il n'y a concrètement qu'un abus de pouvoir. Auprès de sa cour, cependant, la perspicacité du prince passe pour une conception profonde capable d'imposer des écarts.

On alléguera qu'il est impropre de parler d'abus puisque la cour consent (même pour de mauvaises raisons) à se soumettre aux opinions du prince. Toutefois l'abus, bien compris, ne réside pas dans la réinterprétation idéologique du monde telle que le prince parvient à l'imposer, mais dans le cumul d'avantages sans liaison avec sa valeur réelle qu'il capitalise ; avantages cumulés, au surplus, rapportés exclusivement à l'amélioration de son rang.

La Boétie faisait déjà observer que ce type de personnes savaient se fortifier et devenir toujours plus fortes "comme le feu qui se renforce".

La troisième condition enfin est associée à sa légende. Si le prince use de sa perspicacité pour mettre des idées à la mode, c'est à sa réputation qu'il doit son éclat. L'éclat n'est que la mise en valeur d'un coup d'éclat passé, la longue traîne d'une estimation de la valeur du talent du prince à un instant t. Ce talent brièvement entrevu a connu un rayonnement public (quelle que soit la taille du public touché), mais, quoiqu'il soit désormais hors d'actualité, il produit encore des effets. Une telle survivance ne s'explique que grâce à l'existence de témoins. Parmi le public de son coup d'éclat, il se trouvait certainement quelques témoins convaincus de la réalité de la valeur du talent entrevu (sinon tout est à refaire). Ces témoins-là capables d'attester de ce qu'ils ont vu, lu ou entendu octroient au prince un nouvel avantage : celui de n'avoir pas à renouveler la démonstration de son talent. Éludant ainsi la difficulté d'avoir à rééditer un exploit chanceux, il peut désormais vivre tranquillement à crédit sur sa réputation.

Car l'existence du témoin remplace la démonstration de son talent (la preuve) par une conviction, c'est-à-dire un réseau de croyances jamais examinées une à une. Il agit, si l'on veut, comme une espèce de répétiteur de l'exploit premier. C'est une idée chère à l'auteur de la *Psychologie des foules*, Gustave Le Bon, que les croyances s'ancrent dans les esprits à force d'usure, à coups de répétitions.

Le témoin reproduit par son récit le récit originaire où s'enracine le talent du prince et le stylise selon les procédés d'amplification rhétorique naturels à ce genre de récits rapportés. Grâce aux témoins de son coup d'éclat, le prince emporte ainsi la conviction qu'il est un individu de talent. Y compris si quelqu'un venait à produire des éléments mettant en doute l'attribution de ce talent. En effet, un témoignage peut être maintenu contre la vérité, même si ce n'est plus alors qu'une interprétation infidèle des faits.

# Croire ou faire semblant de croire ?

`\textsc{Il y a}`{=tex} deux manières essentiellement d'entrer au service du prince.

Ou bien vous êtes un sot qui avez été témoin de son coup d'éclat (vous en avez une connaissance directe), ou bien vous êtes un sot qui avez entendu parler de son coup d'éclat (vous en avez une connaissance indirecte, par description).

En devenant courtisan, vous rejoignez respectivement le premier cercle, historique, ou les suivants, plus éloignés. Voyez au passage comme cette conception de la loyauté, qui distribue les courtisans par degrés de proximité, introduit subtilement un principe hiérarchique au sein de la cour du prince. Les courtisans sont tacitement mis en rivalité de sorte qu'ils vont se disputer l'accès aux places les meilleures. Ils vont se jalouser alors même, et sans doute le savent-ils, que la logique historique est sapée par les calculs du prince qui, couvrant ses vrais desseins, brouille la compréhension de cette logique au gré de ses intérêts.

Même passive, la collaboration de ses témoins lui est d'un appui suffisant pour réussir à faire croire sans avoir aucunement à se justifier.

Mais comment le prince parvient-il au juste à "faire croire" et agit-il de manière consciente ? Au niveau formel, tout commence toujours par une assertion, un jugement ou un enchaînement de jugements et d'assertions, tout commence par une "déclaration" qui tend les différents types de phrases la composant vers un but unique : prétendre à la validité, passer pour vrai. En déclarant (le concert réussi, le principe du maintien de la société, etc.), le prince ne cherche nullement à alléguer ce qu'il dit puisqu'il n'y a pas d'examen des raisons. Il ne fait qu'"instituer" la croyance que ce qu'il dit est vrai.

Instituer, cela veut dire établir de manière durable, officielle.

Il est tout à fait possible d'ailleurs d'imaginer que faisant cela, il n'agisse pas dans le but d'instituer ce qu'il déclare, mais qu'il le fasse juste parce qu'il se trouve qu'il le veut, sans raison particulière ; or dans le contexte favorable où il évolue, dire ce qu'il dit suffit à produire de la croyance chez ses courtisans. Il n'est donc pas nécessaire de prétendre que le prince ait la volonté d'instituer ses déclarations pour observer, dans les faits, qu'elles le sont.

Le problème auquel nous sommes confrontés maintenant est celui de savoir comment le prince passe, volontairement ou non, d'un régime évaluatif à l'institution de la croyance que ce qu'il déclare est vrai. En fait, l'articulation du dire au croire ne pose de difficultés que dans la mesure où on suppose que les déclarations du prince sont de nature à agir sur les croyances des courtisans et à les modifier réellement et en profondeur. Placé dans l'impossibilité de formuler une loi exprimant la causalité de cette action, il faudrait alors convenir avec le sens commun que le prince manipule l'esprit de ses interlocuteurs, exerçant sur eux une influence occulte. Ou conclure à l'incurable idiotie du courtisan, comprise comme carence intellectuelle.

Je me figure que le pouvoir du charisme tient (au moins en partie) à ce que le courtisan adhère de lui-même aux déclarations du prince, mais qu'il y adhère extérieurement seulement.

En effet, adhérer aux déclarations du prince n'implique pas que le courtisan a été convaincu, il peut bien, quant à lui, rester critique. Seulement, il réserve ses opinions pour lui-même, sachant très bien, s'il tentait de les faire valoir, qu'il ne serait pas en position de les faire briller ni de les imposer. Il n'a ni ce pouvoir ni cette liberté, battu d'office par le prince qui campe solidement sur ses avantages.

En d'autres termes, le courtisan accepte totalement, sans degré, le contenu des déclarations du prince dans un certain contexte alors même qu'il ne croit pas à la vérité de ces déclarations ; il se contente de faire comme si elles étaient vraies. Car donner publiquement l'impression de s'être forgé une opinion éclairée au contact des déclarations du prince est un moyen aisé, sinon profitable d'offrir à ce dernier des gages de loyauté et de consolider par la même occasion sa place au sein de sa cour. Croire équivaut alors à s'engager vis-à-vis du prince.

Replaçons le ressort de cette adhésion dans sa séquence chronologique.

Dans un premier temps, par chance, le prince produit un coup d'éclat qu'il a le talent de mettre en scène : il a donc le talent de mettre en scène son talent (dissimulé).

La structure "gigogne" du talent facilite les glissements de niveaux et les interprétations abusives confondant la mise en scène talentueuse du coup d'éclat avec le talent véritable. Supposez qu'on vous demande ce que vous voyez en désignant le portrait que vous avez devant les yeux. J'ai de bonnes raisons de croire que vous répondrez "Louis XIV", plutôt que "une représentation de Louis XIV". Pourtant, une chose est de commettre une métalepse par raccourci de langage, autre chose est d'exploiter cette confusion à des fins de domination.

Dans un second temps, donc, quelques sots éblouis portent témoignage de la réalité de la valeur du talent du prince de manière indépendante et lancent de la sorte sa réputation dans le monde ; réputation renforcée plus tard par des témoins de second ordre, accréditant le talent du prince indirectement, uniquement sur la foi de sa réputation. L'interprétation légendaire devient légende.

À ce stade, le prince n'est plus seul, isolé dans la prétention de son rang, mais soutenu par une cour homogène (de son point de vue), bien que ses membres gravitent indépendamment les uns des autres autour de leur centre d'attraction.

Enfin, fort de sa puissance, le prince réactive, rallume sa propre légende *ad libitum* en s'efforçant de tirer le maximum de crédit de la réputation de son talent.

Parallèlement, un glissement semblable s'est opéré sur le plan de l'énonciation. Le "je" engageant le prince devient insensiblement, sans degrés ni à-coups, un "nous" où viennent s'agréger tous les suffrages remportés depuis son coup d'éclat initial.

La première personne du prince qui s'exprime n'est pas singulière, mais plurielle ; c'est un "nous" de majesté. Sa voix résonne de toutes les voix du groupe qui lui ont donné leur adhésion. Pourquoi, nous demandions-nous, le prince a-t-il l'assurance d'être cru avant même de parler ? Simplement parce que sa parole a le poids d'une énonciation collective. Il est avantagé par la force du nombre. Voilà pourquoi le prince doit impérativement former une cour autour de sa personne : afin de faire émerger un "nous".

Extérieurement, les courtisans adhèrent à ce que le prince dit qu'il y a, si bien que cette version des faits devient ce qu'il y a : leur adhésion crée un "nous" qui y croit. La version devient alors la seule version, c'est-à-dire une "idéologie".

J'appelle idéologie toute interprétation qui devient vraie collectivement dans le récit officiel du monde, à la faveur de l'adhésion qu'elle suscite, indépendamment des faits. Il y a idéologie quand ce qui est décrit doit exister nécessairement comme cela a été décrit.

# Le talent d'exploiter son talent

`\textsc{Faire partie}`{=tex} de la cour, être un courtisan implique l'obéissance à une obligation minimale du type suivant : croire à l'ontologie du prince signifie, littéralement, croire qu'existe ce qu'il dit qu'il existe. Cette adhésion doit au moins être de façade, c'est-à-dire externe pour créer un récit collectif du monde dans lequel le prince est prince.

Il y a un ensemble de conditions qui, par le biais de paralogismes ou de raisonnements savonneux, facilitent cette soumission volontaire aux déclarations du prince, soumission qui ne souffre, une fois encore, ni opposition ni contradiction (ou à la rigueur de principe pour conforter des positions déjà attribuées).

Ces conditions naissent dans une "matrice", à entendre au sens large de milieu où quelque chose prend racine. Ici, le milieu est le groupe et plus précisément, le groupe au sein duquel le prestige circule archaïquement comme une valeur d'échange. Cette matrice admet le prestige comme monnaie symbolique. En valorisant le prestige, elle joue son rôle de matrice qui est de donner naissance à des centres où cette valeur se concentre. Il faut donc admettre que des rapports d'influence germent et fleurissent chaque fois qu'en un lieu l'économie du prestige supplante l'éthique de la vérité.

Le groupe agit comme une matrice. Il sélectionne certaines conduites en les valorisant, en rejette d'autres en les dépréciant. Le prince ne peut venir à l'existence que dans une matrice où le prestige donne le pouvoir.

Dans cette sorte d'environnement, "avoir du talent" représente l'aptitude de base.

Cette aptitude chez le prince ne remplit qu'imparfaitement les exigences élémentaires de vérification qui seules pourraient confirmer l'existence avérée du talent. Au lieu de se soumettre aux épreuves de vérité, au lieu de clarifier une fois pour toutes la nature de son talent, le prince emploie diverses "techniques" permettant d'exploiter au mieux son talent (dissimulé).

Le mot "technique" doit laisser entendre qu'il s'agit d'un savoir construit par interactions entre histoire, milieu social et objets : une technique s'apprend et se transmet. Au nombre des techniques d'exploitation du talent primaire, il faut compter le savoir-parler (perspicacité, être maître de son langage), le savoir-briller (éclat), le savoir-remémorer (légende) et le savoir-poser (énonciation collective).

Mais disposer d'un talent de base et de techniques n'est pas encore suffisant pour revendiquer son rang. Il faut aussi posséder le talent (secondaire) de bien exploiter les techniques d'exploitation de son talent (de base).

Ce talent-là relève aussi d'une construction, c'est encore une technique : celle d'exploiter efficacement diverses techniques d'exploitation. Par économie de langage, on dira simplement que le prince a le talent de mettre en scène son talent.

Je pense que cet attelage hétérogène de deux types de talent, l'un donné, l'autre construit, alimente la confusion, et il n'est pas dans l'intérêt de ceux à qui cette confusion profite d'en dévoiler l'imbrication. Qui voudrait partager les profits d'un tel hold-up épistémologique ?

Le problème de la genèse et de la nature de l'aptitude qui constitue le talent de base du prince est une question tout à fait secondaire pour notre propos. Nous pouvons nous la traiter de la manière suivante : considérant qu'on comprend l'aptitude au sens large : (1) soit comme une propriété essentialisée, comme un don inné, entérinant l'idée d'une inégalité naturelle entre les individus ; (2) soit comme une propriété produite à partir du social, en fonction des attentes ou des valorisations d'un milieu, élaborée selon un processus sélectif.

Nous nous bornons à dire, relativement à la question qui nous occupe, que l'aptitude est une propriété de l'agent (1), à savoir quelque chose d'assimilable à une curiosité investie, une curiosité à laquelle on donne suite et sens ; et cela, sans égard pour la naissance de cet investissement, libre ou contraint. Soit dit en passant, et aux dépens de la thèse (2), une curiosité peut très bien naître, se maintenir et même se renforcer à l'intérieur d'un milieu qui la décourage fortement (ce qu'illustre par exemple le livre de P. Bergounioux, *Enfantillages*).

Le talent est une notion équivoque qui possède pourtant deux significations clairement distinctes ; mais ces deux sens sont vicieusement intriqués dans le cas du prince. D'un côté, le talent de base né d'une curiosité investie par goût. De l'autre, le talent de second ordre, le talent de mettre en scène son talent de base, qui n'est pas une propriété formée à partir d'une curiosité investie, mais une technique (cela explique au passage que la genèse de cette acquisition puisse faire l'objet d'une enquête sociologique).

La distinction entre talent primaire (curiosité investie, consolidée par l'apprentissage) et talent secondaire (technique d'exploitation du talent primaire) permet de rejeter en bloc toutes les analyses "ensemblistes" qui décrivent le talent comme un cumul intégré de capacités et de reconnaissances, un ensemble de ressources disponibles (Schotté), tout simplement parce que ces analyses ne s'occupent que de la question du talent secondaire.

Or, la composante élémentaire est cruciale. Il faudra s'en souvenir lorsqu'on déploiera l'éventail des "manières" du prince, ces conduites hypocrites de vertu laissant habilement croire que, dans son action, le prince est supérieurement guidé, inspiré par son talent, entièrement sous son empire. Car ces manières consistent à exagérer la part naturelle du talent, à faire constater la supériorité d'un "don" qui laisserait croire à une inégalité naturelle de la répartition du talent.

Là encore, ceci est largement abusif.

# Velléités du courtisan

`\textsc{Tentons maintenant}`{=tex} un pas supplémentaire dans la description des effets de fascination liés au charisme : "courtisan" serait celui qui : (1) vise le talent de base du prince et/ou qui possède un semblable talent, mais sans se l'attribuer ; (2) est plongé dans une matrice où les valeurs de vérité et de sincérité sont méprisées ; (3) exploite le talent second du prince à ses fins propres.

Le courtisan n'est donc absolument pas un sot ordinaire. Lorsque le sot prête l'oreille aux discours du puissant, qu'il suspend les opérations critiques de son esprit, qu'il se laisse aller à agir ou à penser à sa mode (même temporairement), c'est la plupart du temps par paresse ou par désinvolture, par aversion de l'effort. L'attitude de soumission que ce dernier manifeste indique surtout qu'il recherche à vil prix la valorisation de sa personne, la reconnaissance sociale à travers l'imitation du jugement d'une personne socialement distinguée.

Je défends l'idée, pour ma part, qu'à la différence du sot, le courtisan n'ambitionne pas d'être le même type de personne que le prince en singeant ses attitudes et ses déclarations.

En définissant le courtisan comme celui qui, plongé dans un milieu hostile aux vertus intellectuelles, exploite le talent second du prince à ses fins propres, il devient alors possible de distinguer deux caractères chez le courtisan en fonction de ses prétentions.

Le courtisan "avisé", qui connaît le monde et qui veut y percer ; il envie la position du prince et convoite un emploi auprès de sa personne, parfois même sa place. C'est un rival non déclaré qui ne rougit pas de s'abaisser pour tirer parti d'une occasion, qui n'a pas honte de comploter ou d'ourdir des cabales contre son protecteur.

L'autre "droit", qui voudrait être maître de lui-même, mais qui sait qu'il faut s'ajuster au monde ; dépourvu du talent de savoir extérioriser son propre talent dans une société sans vertus, il trouve auprès du prince un appui. C'est un être intègre qui souffre de ne pas être soutenu par son mérite, car partout et toujours il est pris pour un sot.

Je suis parti du postulat que le courtisan possédait le même talent de base que le prince (dans une moindre mesure) ou s'il ne l'avait pas, qu'il le voudrait. Cela m'a permis d'expliquer la raison de cet ajustement réalisé entre eux à travers la figure de l'assentiment. Avisé ou droit, le courtisan ne désire servir le talent du prince que parce qu'il souhaite l'acquérir pleinement pour lui-même (indépendamment de ce qu'il en fera par la suite).

Il est vrai que par moment l'alliance de raison qui naît entre la cour et son prince ressemble à un étroit compagnonnage qui pourrait réduire, s'il était vertueux, l'inégale répartition des aptitudes par l'apprentissage et la pratique. On verrait alors le courtisan aller à l'école du prince pour se former aux techniques d'exploitation de son propre talent.

Mais c'est oublier que le prince, sempiternellement obligé d'abreuver sa légende de prouesses nouvelles, a besoin de se sentir le plus fort et que ce besoin intrinsèque d'acquérir une supériorité, d'acquérir de la distinction, se fait sur le dos de ceux qui travaillent malgré eux à consolider son empire idéologique.

Et le prince ne s'y trompe pas. S'il tient la bride haute à ses courtisans, c'est qu'il n'ignore pas que sous l'abri de sa hauteur ces derniers occupent une place qui est aussi (ou pourrait le devenir) un observatoire de choix pour évaluer ses forces et ses faiblesses dans le but, pour les plus avisés d'entre eux, de lui ravir son talent et sa place.

Le courtisan avisé qui convoite le talent du prince est un imposteur, un prince inaccompli qui dans l'ombre attend son occasion, un rival qui manigance. Ce courtisan-là ne doute pas de sa valeur, mais, obligé à des soumissions qui lui sont odieuses, mortifié d'agir en vassal et résolu à fuir cet état, il doit, le temps de renverser cette disgrâce qui le maintient en servitude, rempocher sa fierté et servir loyalement son protecteur. Convaincu qu'il est moral d'agir immoralement dans un monde immoral, voilà cet intrigant prêt à commettre mille bassesses révoltantes et à user de tous les procédés à sa main. Enfin, il n'hésitera pas à la moindre erreur à reprendre unilatéralement l'assentiment donné, à marquer sa défiance en brisant net l'alliance passée avec le prince pour aller tenter de reformer ailleurs un nouveau cercle (autour de lui), quitte pour cela à inspirer du dégoût. Ainsi, tout en feignant toujours d'ouvrir les yeux à la lumière du prince, ce courtisan habile a recours à des souterrains qui préparent sa revanche de loin --- moment où il pourra revenir sur un meilleur pied. Il veut congédier le prince, mais à son profit. Et comme il sait que ses mauvais offices deviendraient vains s'ils venaient à être découverts, il tâche à ce que son affaire reste obscure. Ce danger permanent sous la menace duquel le prince évolue explique les infinies et tortilleuses précautions que prend ce dernier pour identifier dans sa cour le vrai du faux.

Trop jaloux de son autorité pour en partager ne serait-ce que les miettes, le prince doit très tôt, s'il veut persister dans sa hauteur, apprendre à se méfier de ses rivaux. Toujours craindre qu'un courtisan désireux de le faire chuter puisse s'interposer et parvenir à accaparer sa petite prospérité. Le risque de nourrir un serpent dans son sein est trop important pour ne pas s'en prémunir. Or, face aux ambitions cachées de compétiteurs si intimement côtoyés, qui ont eu par conséquent tout le loisir d'éplucher à vif la construction légendaire de sa personne, il ne sert à rien de remettre en scène ses prétentions une fois de plus. Une telle mesure, purement préventive, resterait sans effet, car l'ambitieux rival cherche à attaquer le prince à la racine. Il est capable de se parjurer, de trahir et n'hésitera pas, au moment opportun, à rompre les liens de loyauté qui avaient jusque-là structuré leur relation.

Pour éviter de se faire contourner de la sorte, le prince peut par anticipation et par prudence ériger deux lignes de défense.

La première consiste à trier parmi ses courtisans ceux qui sont fiables de ceux qui ne le sont pas. Pour cela, le prince multiplie les épreuves de loyauté. Une épreuve de loyauté est une parenthèse codifiée pendant laquelle le courtisan renouvelle, par son attitude, les signes d'allégeance adressés au prince et, par son tour de langage, démontre non seulement qu'il maîtrise le récit officiel du monde, mais surtout qu'il y adhère. Cette épreuve formalise, en la réitérant, la figure de l'assentiment. Naturellement cette exigence, de vitrine, ne met pas le prince à l'abri de la traîtrise, seulement elle en augmente le prix. Il coûte alors davantage d'être perfide.

L'autre ligne de défense tient à l'organisation rigoureuse de ses ressources. La cour, en vertu du principe de division, n'est jamais réunie au complet. Ce faisant, le prince se protège de l'ambition de ses courtisans en couvrant leur position d'un épais voile d'ignorance. Ni l'extension de la cour ni sa composition complète n'est jamais connue du courtisan qui se voit donc privé de la possibilité de se situer. Avec entre autres conséquences le fait que l'ignorance : (1) décourage l'initiative individuelle en faisant craindre la désapprobation générale (insécurité) ; (2) crée des retards choisis, des décalages dans la diffusion du savoir officiel (désinformation) ; (3) transforme chaque signe, chaque mot en énigme requérant de vains efforts d'interprétation pour tenter d'y comprendre quelque chose (paranoïa) ; enfin (4) fait vaciller la certitude concernant qui l'on est et l'estime dans laquelle on est tenu (suspicion).

# Incarner le désordre

`\textsc{Ces verrous}`{=tex} sont les instruments d'une tactique raisonnée dans la lutte contre les trahisons, ces retours de feu toujours possibles aux conséquences redoutables. Mais le déploiement de ces stratégies de contrôle n'offre pas simplement une batterie d'armes supplémentaires à l'arsenal du prince. De tels contrepoids sont en réalité purement défensifs. Simple combinaison de moyens qui doivent l'aider à dresser un cordon sanitaire entre lui et ses adversaires non déclarés. Ces manœuvres ne sont pas destinées à accroître sa domination, mais à écarter toute velléité de rébellion, à brider les prétentions indues.

Cela suggère que la vigilance du prince est extrême quand il s'agit d'endiguer les débordements d'ambition de ses rivaux, leurs menées pleines d'espérance. Or le meilleur moyen de mettre en œuvre sa défense, c'est de semer le trouble, de brouiller le voile clair des certitudes, d'organiser le déconcertement général ; bref, d'incarner le désordre. Les seuls barrages capables de résister aux assauts de la contestation sont en effet ceux qui tirent une couverture sur la nature réelle des déclarations et des agissements du prince. Ce sont ceux qui font obstacle à l'exigence de vérité. Comment ? En jetant les courtisans dans l'embarras, dans le dépit, en frustrant leur désir de réussir, en leur fermant les portes, en moquant leur décence. Comment ? En leur récitant les vieilles leçons du scepticisme, en donnant du poids à certaines choses et du même coup à leur contraire, en leur refusant le droit de savoir, en leur faisant connaître les affres du doute.

Détaillons rapidement ces quatre stratégies (insécurité, désinformation, paranoïa, suspicion).

L'une d'elles (1) consiste à isoler le courtisan de l'opinion générale de façon qu'il ne puisse connaître l'avis majoritaire, à obscurcir la carte intime de la cour. De la sorte, quand un courtisan se risque à prendre la parole plutôt qu'à suivre, à mesurer ses idées à ceux avec qui il est, il se trouve brutalement projeté en territoire inconnu, privé de l'amitié de ses familiers, isolé contre le groupe, certain d'être désapprouvé, sauf à être capable de faire cavalier seul contre des convictions partagées à une très large majorité. Mais plus encore que la crainte d'être désapprouvé, c'est la peur d'être dévalorisé qui domine ; peur diffuse, sentiment de ne jamais se voir à l'abri, climat, brouillard sciemment versé dans le but de diluer la parole adverse, d'en restreindre la liberté, ciel orageux d'où jaillit parfois comme la foudre une pointe, une pique que met le prince. Car, fort de ses avantages, le prince a le pouvoir d'assener des ridicules. En quoi consiste un "ridicule" ? À arracher une phrase de son milieu épistémologique (vrai - faux) pour la transplanter dans un milieu normatif (bon - mauvais). À demander comment on peut penser, oser penser X au lieu de discuter de la validité de X. En un mot, à insinuer au lieu de raisonner. Le courtisan est alors obligé de céder ses compétences sans possibilité de les faire valoir ni même de les essayer, l'expérience lui ayant fait comprendre qu'il serait toujours vaincu. Il n'a alors plus qu'à s'effacer, rempli de dégoût.

Une autre stratégie (2) porte sur le contrôle de l'information. Pour défendre sa prééminence, le prince ne donne au courtisan qu'un accès partiel ou lacunaire à la version officielle de son récit du monde. Mais il peut aussi la modifier sans préavis, capricieusement, par excentricité dans le but de déstabiliser les certitudes du courtisan. De l'une ou l'autre façon, cette tactique vise à inférioriser le courtisan en instillant chez lui la croyance qu'il doit combler un retard qui se serait creusé soit durant son absence soit dans la compétition qu'il mène à distance avec les autres membres de la cour, croyance qu'il doit mettre à niveau ses connaissances au sujet de ce qu'il convient désormais de penser, de dire ou de croire. Il fait peser sur lui toute la difficulté d'aller aux nouvelles. À cause de cet état où il doit sans cesse rabouter l'information à partir de bribes de sens elles-mêmes incertaines et sujettes à modification, le courtisan n'a d'autre choix que de faire connaître publiquement son désir de savoir, d'autre choix que de réclamer d'être instruit, que de mendier pour connaître ce qu'on lui refuse. On vise, en le campant dans la posture d'un homme qui demande, à infantiliser son comportement tout en neutralisant par la même occasion son potentiel de nuisance.

Une autre stratégie (3) a trait au maniement des règles d'interprétation, les actes de communication du prince comportant à dessein de multiples zones d'ombre. Parfois le prince parle en laissant le sens suspendu. Ses mots sont coupés, il sait et se retient ; ou alors il parle sur le ton des oracles avec une emphase divinatoire. La référence de ses propos est tue, volontairement dissimulée, ce qui altère aussitôt leur signification. Incomplets, ces énoncés sont également invérifiables, sans solidité quelconque. La responsabilité de leur compréhension incombe dès lors au courtisan qui, extérieurement du moins, doit, sous peine de s'exclure de l'intelligence commune, produire l'effort de ne pas les considérer comme non fiables. D'autres fois, le prince s'oppose à la reformulation de ses énoncés. Ce faisant, il conteste le droit d'exercer une traduction de contrôle visant à clarifier les contenus formulés indépendamment du ton grandiloquent sur lequel ils ont été proférés ou simplement à demander, plus modestement, confirmation de la compréhension correcte de ce qui a été déclaré. Dans une occurrence comme dans l'autre, le prince fait peser la charge de l'interprétation sur les épaules des courtisans, en se gardant d'étayer leur commentaire pour paraître en dire "plus". Pour lui, tout consiste à rendre sa parole difficile à écouter. Car plus elle sera difficile, plus elle paraîtra profonde, et plus ses vues sembleront sagaces, aiguisant de ce fait, au-delà de toute raison, l'intelligence de ceux qui voudraient comprendre et qui pour cela poussent l'interprétation jusqu'aux limites du délire. Rien dans l'interprétation, aucun *feedback* ne vient arrêter la substitution possiblement infinie des signifiés (qui renvoient toujours à d'autres signifiés et cela, en l'absence de référents, sans fin). Le courtisan est séduit par un reflet dans lequel il s'efforce de voir la chose même, alors qu'il est tout bonnement pris dans un jeu de miroir.

Une autre stratégie enfin (4) s'ingénie à faire connaître le double visage du prince. Ce dernier, pour apprendre à sa cour qu'il peut changer de visage à volonté, s'évertue à ne pas tenir le même discours en public et en privé. Il réserve ses "croyances vraies" pour lui-même et quelques rares élus qu'il aura mis dans la confidence. En public, il loue ou blâme, approuve ou condamne sans qu'il soit possible de savoir quel degré de croyance il accorde à ses propres croyances. Il découle de ce point que la confiance du courtisan dans les attributions formulées par le prince, dans les grâces, ou les qualités, ou les faveurs distribuées en public devient de plus en plus friable. Car rien n'est plus simple que de saper la confiance en une croyance. Il suffit d'affaiblir sa valeur de vérité. Il suffit de faire en sorte que le contenu de cette croyance se révèle faillible. Voilà pourquoi dans la nuit du secret, seul ou avec ses élus, le prince révoque les attributions partagées au grand jour quelques heures plus tôt. Le courtisan n'étant point averti de ce que le prince pense au fond est délégitimé par cette "contre-conversation" (comme on dit un "contre-feu") qu'on allume dans son dos. Contre-conversation à laquelle le courtisan n'a jamais accès, mais qu'il doit toujours pouvoir deviner, sentir, soupçonner, dont il doit pouvoir apprendre l'existence de manière indirecte ou par bribes. Tactiquement, le procédé de double attribution se combine à merveille avec le principe général de division. Ainsi, le prince pourra faire une charge contre un courtisan (C1) et mettre (C2) dans la confidence. Puis charger (C2) et mettre (C3) dans la confidence, et ainsi de suite. Il peut même créer des boucles à son avantage. L'affaire est que le prince peut de la sorte, sans paraître aucunement se dédire, souffler le chaud et le froid sur l'estime de ses courtisans et contrôle à distance leur ascension.

En théorie, suivre ces lignes avec discipline devrait défendre efficacement le prince contre toute tentative de conspiration. Appliquée scrupuleusement, cette approche donne en effet d'excellents résultats avec les "bons" courtisans tout en dégonflant l'ambition des "mauvais". Le bon n'osera pas agir (parce qu'il est droit) tandis que le mauvais aimera mieux différer son action (parce qu'il est avisé).

Pourtant, rien dans les faits ne peut exclure une trahison puisqu'il suffit à chaque courtisan, bon ou mauvais, de détruire la figure de l'assentiment pour être quitte de sa servitude. C'est pourquoi le prince a tout intérêt à compléter l'action curative --- remédiant à un mal --- de son programme de soumission par une action prophylactique qui prépare le courtisan à remplir son rôle. Pour produire ses fruits, l'action du prince doit être menée très en amont, le plus précocement possible, et prendre la forme d'une éducation destinée à inciter tous les nouveaux prétendants à se conformer au canevas idéal de ses attentes.

Enseigner au courtisan les usages du milieu, le former, façonner sa charpente, sélectionner ses meilleures tiges, c'est le rendre par la suite propre à employer en beaucoup de choses et à servir utilement. C'est aussi pouvoir l'éprouver avant de l'admettre. La longue tradition des "miroirs aux princes" s'inscrit dans ce cadre-là.

# Éduquer à servir

`\textsc{Castiglione dit}`{=tex} que le bon courtisan sait se transformer en prince. Il y a du mimétisme de l'un à autre. C'est évident. Toutefois il y a deux manières de comprendre cette acclimatation.

Ou bien on pense qu'il s'agit d'un biais de sélection parce que "le prince n'accepte que celui qui lui ressemble et qui, par ses mérites et ses agréments, lui propose le reflet de sa propre valeur" (Starobinski). Autrement dit, tout courtisan ressemble ou finit par ressembler au prince dans la mesure où ce dernier cherche à se voir et s'admirer dans l'opinion que ce courtisan lui renvoie. Le prince favorise la parole qui le met en valeur, avant de l'essentialiser en jugement universel. Alors la règle de base semble être "paroles contre faveurs". Ce qui correspond en gros aux pratiques de la société aristocratique d'Ancien Régime.

Ou bien on insiste, comme je m'y emploie, sur l'aspect adaptatif du courtisan ("se transformer en") montrant que celui-ci n'est pas seulement sélectionné par le prince sur la base de comportements encouragés, mais qu'il gagne sa place au prix d'une éducation au cours de laquelle il apprend à intégrer le jeu de pouvoir du prince par la pratique, à tâtons, en se formant sur l'exemple, sans jamais suivre de règles puisque la seule règle du pouvoir, c'est de ne jamais énoncer celles que l'on observe. Les paroles et les actes du prince semblent "établis en l'air", selon le mot de Saint-Simon, et ne pas avoir de véritable existence. Description qui correspond d'assez près aux survivances de pratiques aristocratiques dans les milieux contemporains, tout en restant vraie pour la société d'Ancien Régime.

L'assentiment donné implique qu'à partir du moment où le courtisan entre dans la sphère d'influence du prince, il va apprendre à s'ajuster à la personnalité du prince par un continuel et constant effort d'adaptation. C'est un devoir qui lui échoit.

L'assentiment est immédiat, l'ajustement infini.

Deux raisons au moins peuvent expliquer que l'éducation du courtisan ne s'achève, n'aboutisse jamais à une forme de reconnaissance durable. D'abord, les attentes et les goûts du prince sont susceptibles de changer à tout instant puisqu'il doit paraître ne point suivre de règles. Le courtisan est alors obligé, s'il veut plaire, et il le doit, de rattraper le déséquilibre induit par la versatilité de la conduite princière en courant derrière l'information, la nouveauté ou la mode. Ensuite, l'apprentissage de la courtisanerie est un perfectionnement qui n'a de cesse, un surpassement de soi dont le principe se résume à faire toujours mieux, donc toujours plus. Occuper la place de favori, celle que les courtisans veulent à l'unanimité puisqu'elle donne accès aux premières loges de la représentation et permet d'entrer dans la familiarité du talent, occuper cette place, donc, requiert un effort de vigilance incroyable qui va bien au-delà de l'exercice de confiance qu'il a fallu déployer pour la mériter.

En effet, quand il se trouve au comble de la faveur, le courtisan doit encore exercer une grande assiduité à le rester de crainte sinon de tomber en profonde disgrâce. Ce dernier ne connaît, même en cas de réussite, ni la satisfaction d'avoir touché au but ni la sécurité d'une protection dûment acquise. Inlassablement il doit faire ses preuves, donner, prodiguer, faire de nouvelles offrandes de soumission, faute de quoi il s'expose vite à devenir inutile.

La première qualité d'un bon courtisan se borne à intérioriser son infériorité, qualité qui dans la bouche du prince se dit "simplicité".

Le courtisan ne doit pas reprendre, ne pas redire, ne pas se plaindre, ne pas vouloir faire, ne pas rompre un discours. Il lui faut se soumettre de bonne grâce aux opinions du prince qui devient en quelque sorte le maître de son esprit en lui ôtant toute licence d'agir ou même de penser, et l'oblige à tout supporter avec silence. En un mot, le courtisan doit faire le sacrifice de sa personne. La simplicité, en ce sens, correspond à l'effacement public du courtisan qui en société doit se conduire avec modestie et discrétion, ayant garde de ne pas faire de sorties épouvantables ou de scènes éclatantes. On le dit "simple" dans la mesure où il rentre dans le rang, où il sait toujours être à sa place, sans oser jamais dire une parole trop libre ni trop salée ou paraître trop savant, sans jamais être pesant. On le dit simple parce que, comme il manque de conversation, ou que sa conversation est aride, ou qu'il prend toujours le parti de se taire, on le croit idiot, doté d'un esprit porté au petit. On le dit simple, car pour qu'on le remarque il faut que le prince condescende à le rendre intéressant. Il occupe le sot emploi de servir les intérêts des puissants et se fond, se confond avec cet état.

Mais cette servilité n'est pas tout à fait exempte de calcul, on l'a dit. C'est un type de troc : soumission contre protection, un marché de l'ombre où le fait d'être à couvert, embusqué, dans l'angle mort du pouvoir confère paradoxalement des libertés supplémentaires, celle par exemple de se former par la conversation aux règles de ces jeux de pouvoir pour les exploiter à son tour.

Il faut aussi que le courtisan bien éduqué apprenne à faire preuve de prévenance, qu'il apprenne à voir dans le prince un homme qui réclame des égards. Cet homme digne de recevoir des respects mérite naturellement d'être traité comme une élite. Quel que soit son propre caractère, le courtisan se montrera donc attentif à plaire. Plaire par sa conduite, plaire par ses paroles, par le ton de son langage, en distribuant des marques d'approbation. Au cours de ses multiples palabres avec le prince, le courtisan veillera ainsi dévotement à ne rien dire qui ne soit de son goût. Il ne flattera pas le prince, mais louera sa finesse de vues. Car de l'affirmation d'un goût naît l'acte de puissance : il faut aimer et faire aimer ; puis faire craindre de ne pas aimer comme il faut. Faire ployer sous son goût.

Et ce goût doit s'entendre ici dans le sens le plus ordinaire du langage comme une inclination ou une attirance pour tel type de X où X désigne alors un style de personne, une manière d'être ou de faire, ou le produit de ces manières. Qu'importe X au fond, dès lors qu'il paraît désirable.

Aussi, pour s'accorder à l'humeur fantasque du prince, le courtisan développera une attention particulière à ses goûts, une sorte de souplesse d'esprit qui doit permettre de suivre leur évolution constante et leur revirement possible. Encore faut-il être capable pour cela de sentir l'essor d'une préférence, c'est-à-dire d'aller au-devant de la curiosité du prince, d'anticiper ses plaisirs, deviner le mot qui fera rire, etc.

À la fin, stade suprême de la soumission, il faut faire tant pour ménager le jugement du prince que ce n'est plus par prévention, mais par crainte de paraître brutal et d'être grondé que le courtisan se montre aimable. Il ne se soucie plus de favoriser l'agrément du prince, mais de ne pas l'aigrir. Entre vouloir plaire et ne pas vouloir déplaire, l'écart est ténu. Il y a pourtant un saut qualitatif énorme, qui correspond à l'acceptation d'une norme d'autocensure.

Quelque chose, à force de prévenance, s'est inversé dans la dynamique de leur liaison. Et tout se passe alors comme si le courtisan détenait le pouvoir de meurtrir le prince et qu'il devait éviter par conséquent d'agir d'une manière fâcheuse. Le prince, lui, à qui il suffit de faire savoir qu'il serait extrêmement blessé d'en entendre parler davantage pour imposer un silence définitif sur une matière, se pose comme arbitre de cette norme, se faisant lui-même justice de décider ce qui peut se faire ou se dire. Le prince s'assure de garder le contrôle de leurs échanges en faisant adroitement sentir au courtisan qu'un mot de trop serait capable de gâter leur amitié.

Enfin le courtisan doit apprendre à se montrer digne de la confiance accordée. Il n'y a aucune raison de penser que cette dignité lui soit acquise. Parvenir à théâtraliser sa satisfaction quand le prince condescend à relever le mérite de l'un de membres de sa cour, donner à voir sa reconnaissance est l'une de ces qualités qui s'apprend à force d'immixtion dans les relations de pouvoir. Il doit apprendre à exprimer sa gratitude et à ouvrir son cœur quand le prince vient à lui témoigner sa considération.

Or cette considération, bien qu'espérée, n'en est pas moins un jeu de dupes. Le mérite reconnu au courtisan n'est jamais un compliment adressé à sa valeur personnelle, mais à sa qualité d'imitation, à sa capacité de continuer par ses moyens propres la guerre idéologique du prince. D'ailleurs, d'une manière générale, toutes les politesses du prince valent pour des rétributions symboliques de la vertu du courtisan, quand cette vertu s'accorde aux valeurs du prince. Quand ce dernier témoigne des égards exclusifs à son auditoire ("toi au moins tu..., pas comme..."), c'est évidemment pour que l'on s'attache à sa personne en donnant à chacun le sentiment d'être "l'ami particulier", un honneur partagé entre peu de personnes, le courtisan doit être comblé de la préférence qu'on lui donne, et le faire voir.

En fait, la considération du prince ne dit jamais autre chose que cela : "Tu as fait ce que j'attendais de toi." C'est ce dévouement, cette abnégation qui lui est comptée : avoir été capable de défendre le récit officiel du prince, de colporter ses "vérités", celles qui sont bonnes à croire, être devenu une pièce rapportée de son réseau.

Le "compliment" (entre guillemets puisque c'est une hypocrisie) approuve la compréhension correcte et la bonne anticipation de ses attentes. Il valide la juste interprétation des valeurs implicites de la narration commune. Il célèbre l'aboutissement d'une formation réussie. Il valorise le courtisan relativement aux intérêts du prince, mais non dans l'absolu.

Mais c'est encore un jeu de dupes parce que le courtisan est tenu de remercier le prince de ce compliment. Il faut exprimer sa gratitude. Il faut faire voir qu'il a conscience que ce compliment est une marque de distinction, une médaille qui augmente son crédit auprès du prince et des autres membres de la cour. Il faut montrer qu'il mesure le gain réalisé, montrer qu'il perçoit comme un privilège la confiance accordée. Ce témoignage de reconnaissance formulé en contrepartie est l'acte qui va le rendre digne de l'honneur décerné.

Mais qu'induit-elle, cette reconnaissance ? Une dette.

Le courtisan complimenté ne doit pas, ne peut plus décevoir. Donc, au lieu d'acquitter le courtisan, la reconnaissance l'endette. Et qu'est-ce qu'une dette ? Une obligation de remboursement. Un service à devoir. Une servitude nouvelle.

En vérité, ce compliment resserre étroitement les liens qui l'enchaînent au prince. Un dû --- pour qui regarde à la vertu --- oblige à l'exemplarité, à l'intégrité, à la droiture. Aussi, faut-il voir la dignité comme une dette d'honneur, et la volonté d'éteindre cette dette, dans une large mesure, comme origine de la loyauté.

Le compliment qui récompense la faculté d'assimilation des valeurs idéologiques de la cour fait entrer le courtisan dans le costume d'un homme de qualité. La confiance et estime dont il peut désormais s'enorgueillir va cimenter peu à peu les bases de leur association, et ce nouveau marché : compliment contre dignité, recouvrir lentement l'assentiment qui l'a rendu possible. Le compliment est un instrument utile et efficace pour pérenniser la conduite du courtisan, car il le fixe dans le rôle étriqué de la personnalité éternellement méritante. Une fois les valeurs de la cour sues, une fois sa dignité gagnée, le courtisan est obligé, en vertu de sa dette morale, d'agir avec une grande uniformité de conduite qui implique la probité, la franchise, le respect de la parole donnée, le sens de l'engagement jusqu'au sacrifice. Autrement dit, il doit agir loyalement.

Cette fidélité sur laquelle le prince sait pouvoir compter est le signe et le gage de la soumission morale du courtisan. Pourtant, elle n'est pas que cela. Pas uniquement un dû qui découle de la reconnaissance de son mérite. Elle permet aussi au courtisan de se prémunir en théorie d'un acte de trahison toujours possible. Son comportement loyal le met à l'abri des humeurs versatiles du prince : il le protège de son protecteur. En effet, un prince hésitera avant de se résoudre à tromper un homme qui se fie à lui. La loyauté du courtisan lui permet aussi, quoique pour de mauvaises raisons, de s'affranchir du doute et de la crainte vis-à-vis des caprices du prince.

Voilà pour les qualités du bon courtisan --- simplicité, prévenance et loyauté.

Elles s'apprennent à force. Sans injonction ni contrainte. Exister à la cour, c'est faire cette expérience continuelle où les préférences se déchiffrent à travers un système de récompenses symboliques qui fait aussi partie des techniques de soumission. L'efficacité de ces techniques tient, on l'a vu, à leur cohérence, au fait qu'elles forment un réseau interdépendant de ressources capable de compenser une défaillance par une ressource parallèle. Or ces compensations, ces flottements, ces enchevêtrements de niveaux, ces boucles autoréférentielles, en un mot toutes ces torsions faites à la chaîne des raisons servent le même but : tisser ce prodigieux cocon destiné à métamorphoser le néant en charisme.

Le prodige ne se limite pourtant pas à fabriquer des croyances qui soumettent. Le prince séduit aussi, en plus des arrangements qu'il prend pour briller, avec ses manières dont il reste maintenant à dire quelque chose.

# Façons d'agir

`\textsc{Par "manière"}`{=tex}, je désigne toutes les façons d'agir qui ne font pas partie des techniques de représentation du talent de base, quoiqu'elles soient indirectement affectées au même but : consolider la croyance en l'innéité de ce talent, orienter la pensée d'autrui par de fausses évidences. Elles n'appartiennent pas à la technique parce que leur emploi n'est ni enseignable ni méthodique. Mais aussi parce qu'elles ne font pas l'objet d'une maîtrise consciente. Cela ne signifie pas que les manières n'aient pas une origine socialement construite.

Pourrait-on d'ailleurs vraiment soutenir une telle idée ? Je me contente d'observer que ces manières, sans considération pour le processus qui leur a donné le jour, finissent par coller de si près à la personne du prince que c'en est comme une seconde peau. Si le talent autorise la manière, la manière, elle, renforce l'idée du talent.

Avoir des manières, c'est d'abord ne songer jamais que l'on en a. Ces façons d'agir composent pour l'assistance un extérieur plus vrai que nature, un contour dessinant une personnalité unique, rendant le prince plus superbe qu'il ne l'est à la lumière de son talent. Elles renvoient de lui une image aimable sans pourtant y paraître le moins du monde. Elles grandissent son talent sans ostentation. Elles paraissent couler de source. Ces manières concourent à le faire paraître distingué et spontané, fin et naturel, audacieux et amical. Car avoir des manières, c'est ne pas s'inquiéter inutilement de n'être pas approuvé, se savoir invincible, ne douter de rien, et encore moins de soi. C'est une façon d'aller, d'avancer dans l'existence, de franchir les obstacles, en un mot c'est une "allure".

Je propose de décliner les manières suivant trois types de conduites, trois comme les trois allures du cheval --- pas, trot et galop. Comme n'importe quelle taxinomie, cette division repose sur un principe arbitraire, amendable, révocable, mais qui me semble opérant. Parmi ces façons d'agir, je distingue donc la manière "négligente", la manière "plaisante" et la manière "connivente" qui ne sont pas exclusives l'une de l'autre. Bien au contraire, elles se combinent et s'assemblent merveilleusement.

Par certains côtés, la manière "négligente" peut se rattacher à la *sprezzatura*, ce détachement feint, cette nonchalance dont Castiglione écrivait qu'elle était l'une des principales qualités de l'homme de cour.

Pour Castiglione, cette qualité réside dans l'art de "dissimuler tout art", de faire oublier ou encore de dépasser la technique pour faire croire que "ce que l'on a fait et dit est venu sans peine". C'est une désinvolture étudiée, une retenue.

À l'inverse, il me semble que chez le prince la manière consistant à s'approprier tout avec facilité n'est pas un art, mais une seconde nature et qu'il ne faut pas la regarder comme le surpassement d'une technique par intégration de sa technicité (le paradigme de ce surpassement étant peut-être incarné par le musicien dont le jeu se libère de l'exécution purement technique de sa partition). Or le prince, qui pousse son avantage sans rencontrer ni résistance ni opposition, se contente d'évoluer de façon opportuniste. Il n'a pas besoin de travailler à persuader puisqu'on vient lui manger dans la main. L'air d'aisance qu'il arbore est le corrélat mécanique de l'accumulation de toutes les concessions qui lui ont été faites de tous bords. Elle occupe le champ laissé libre par l'absence de contradicteurs, par leur démission collective. Cette apparente facilité n'exige à proprement parler aucun "travail", aucune dissimulation : elle fleurit simplement sur les lâchetés de son auditoire. Elle lui vient réellement sans effort, ce n'est pas une impression ni un effet concerté, car le prince sait d'expérience qu'il peut négliger publiquement l'opposition, à savoir compter sur le fait qu'elle aura un poids négligeable.

La manière "plaisante" est déterminée par la propension à susciter et à diffuser le rire. L'esprit de plaisanterie qui le caractérise est à entendre au sens large. Il tient aussi bien du mot lâché comme une bombe que de l'effronterie typique du vaniteux qui se débarrasse avec décontraction de l'objet ou de la personne qui fait obstacle à ses idées en déversant sur lui un tombereau de risées. D'une part, la saillie à tous les degrés --- de la badinerie la plus légère à la repartie la plus cinglante --- étaye l'impression de perspicacité qui découle de ce don de la parole. Cette éloquence naturelle comble l'auditoire qui sait gré au prince d'être tellement à l'aise. La cour peut ainsi avoir l'agrément de participer à un entretien ou à une conversation qui donne de la surprise, et prendre du plaisir, celui de vagabonder dans un pays étonnant où tout semble à la fois familier et méconnaissable, tout en voyageant à peu de frais. Le prince règne en maître de la conversation. C'est pourquoi on loue sa vivacité, la justesse de ses jugements, son art d'amuser, tout en se berçant de l'illusion de raisonner d'égal à égal avec lui.

De l'autre, quand il tend à devenir moquerie, le trait perçant rassemble sur celui qu'elle exclut. Le prince ne retient plus ses propos, il les lâche. Le sarcasme, ce noir artifice de l'orgueil, soude alors la cour dans l'idée de sa supériorité. L'essence de ce rire est "diabolique", comme dira Baudelaire ; ce rire se goberge de sa méchanceté hissée au rang de lucidité suprême. Les mauvais succès d'autrui, les insuccès, les succès illégitimes attisent la flamme d'un rire fier. Rire est le symptôme d'une humeur enragée qui intimide et fait craindre à celui qui s'en rend complice d'en expérimenter une fois l'épine. Toujours, et dans toutes ses nuances, douces ou cruelles, la manière plaisante trahit un comble d'infatuation. Par le rire, le prince conforte son clan et espère encore gagner les indécis à sa cause. Unique obsession de ses sorties publiques : augmenter l'estime universelle au sujet de la naturalité de son talent.

Enfin, la manière "connivente" renvoie à l'air arrangé, à ce ton pénétré que prend le prince en public, tout particulièrement quand il doit intervenir de manière officielle. Lors d'une telle épreuve, le prince paraît nu, sans sa cour (même si celle-ci est mêlée au groupe). Pour limiter son exposition et dissimuler sa vulnérabilité, il est dans son intérêt, le temps de cette épreuve, de souder autour de lui une cour de circonstance. Il peut certes compter sur le fait que collectivement l'assistance est acquise à son rang (sans quoi elle ne serait pas là), mais il ignore quel est l'état d'esprit des individus qui la composent. C'est pourquoi il va tenter de passer d'éphémères alliances en captant son public par les moyens de la connivence, affective ou intellectuelle.

C'est par exemple cet orateur qui interrompt tout à coup sa communication pour rapporter en façon de confidence un mot d'enfant, le mot de son fils, Paul (Pierre ou Jacques), 3 ans, ce matin-là, et qui, pour conclure cette émouvante séquence, fait planer un silence long et lourd de sérieux. L'élément de sens, la leçon, ce qu'il faudrait apprendre de ce conte charmant enfin, est laissé en suspens, en délibéré, offert à la sagacité du public forcément ravi, enchanté par ce déballage offrant une vue imprenable sur l'arrière-cour de l'orateur.

On dira ainsi que le prince parle par allusion chaque fois qu'il désigne de manière oblique un savoir qu'il ne veut pas nommer ouvertement, chaque fois qu'il prend le parti d'agir ou de parler, surtout de parler, de façon voilée, chaque fois qu'il dit quelque chose par d'autres moyens.

C'est encore par exemple cet auteur qui, voulant réfuter la thèse d'un philosophe américain, la disqualifie sans discussion au moyen d'une amusante didascalie : "*(rires !)*" Ce procédé relève aussi bien de la négligence --- par l'attitude cavalière et familière qu'indique une telle façon --- que de la connivence. En l'occurrence, l'auteur suppose que le lecteur sait (aussi bien que lui) pourquoi la thèse de Quine est risible et peut-être même digne de railleries. On voit au passage combien ces manières sont accordables entre elles.

Revenons un moment à la logique de l'allusion exposée ci-dessus et voyons pourquoi elle est l'une des manières du puissant. Pour cela, il faut accorder au sens commun ce point : l'objet du sous-entendu est une référence implicite. Mais le terme "référence" ne désigne dans le cas du prince ni un renvoi à un second niveau de sens ni un retour sur énoncé si bien que l'objet de la référence n'est ni une interprétation ni une citation. C'est plutôt quelque chose comme un réseau d'informations et de croyances auquel on accède (ou non) par inférences et qui représente, sous condition d'opacité, ce qu'il convient de penser du point de vue de l'énonciateur, autrement dit un fragment de sa mise en récit du monde.

Dès lors, le surcroît de puissance qui découle de cette façon d'agir procède de la sorte : ou bien l'allocutaire décrypte la référence implicite, alors il appartient à la caste élue, et peut s'enorgueillir de posséder un secret bien rare ; ou bien il la manque et l'accès à ce savoir lui est refusé. S'il la manque, ou bien il peut feindre de l'avoir décodée, mais il est alors à la merci d'un ridicule, car l'exposition publique de son ignorance lui serait une grave avanie, ou bien il fait spontanément aveu d'ignorance et réclame l'objectivation de ce qui lui a échappé, mais il envoie alors un signal fort de soumission.

La dissimulation des sources sur laquelle repose la manière allusive gagne ainsi sur tous les tableaux. Soit la référence est identifiée, et elle instaure une connivence, soit elle ne l'est pas, nul n'ose faire commentaire, et la soumission s'impose alors dans un silence respectueux.

# Combattre la fascination

`\textsc{Au terme de ce panorama}`{=tex}, une question pourtant demeure sans réponse : la fascination peut-elle être combattue ?

Dès lors que le courtisan défère sa liberté de jugement à une autorité prétendument mieux avertie, même si intérieurement il préserve l'intégrité de son sens critique, il est mis sous tutelle, au sens de Kant. La diminution de son autonomie intellectuelle est une forme de servitude volontaire, avec ses gestes et ses façons. Le rapport de force est entériné, puis perpétué sur la base de signaux échappant à l'interprétation, manifestations anté-symboliques caractérisées par leur totale transparence, leur non-dissimulation, leur sincérité, puis conforté, solidifié et finalement fixé par les interactions verbales qui viennent se greffer par-dessus. À qui sait les voir, pour qui veut les exploiter, ces signaux indiquent d'un côté l'opportunité de prendre l'ascendant, de l'autre le désir de recevoir un appui. Peut-être faut-il admettre en fin de compte, malgré toutes les réserves qu'on peut avoir, que ces signaux jouent un rôle de premier plan dans la formation de la figure de l'assentiment. Une fois passé ce seuil critique, le prince déroule. Avantagé par le cumul de techniques en sa possession, il ne fait au fil du temps que resserrer son emprise. Et bien que poursuivant des buts divergents, le couple asymétrique composé par le prince et sa cour forme une alliance heureuse, capable de loyauté et de se maintenir dans le temps, de perdurer en vertu du gain social réalisé pour les deux.

Seulement, ce bonheur a un prix pour le courtisan : celui du compromis.

Le bon courtisan, celui qui n'ambitionne pas le pouvoir, doit apprendre à vivre en contradiction avec ses convictions, apprendre à dissocier sa pensée de sa parole, apprendre à faire passer le récit commun par-dessus ses croyances. Mais lorsque cette torsion devient intolérable, comment recouvrer l'indépendance ?

Sortir de l'état de minorité implique obligatoirement de quitter la cour du prince. Le courtisan qui avait dû passer sous les fourches caudines de l'assentiment pour entrer dans la familiarité du prince ne peut, en raison de son inconditionnalité, renégocier cet assentiment d'aucune manière que ce soit. Cela exclut le recours à la raison comme possibilité d'aplanir un litige, étant donné qu'il n'y a pas litige, mais consentement. Cela exclut le recours à un arbitrage. Cela exclut la possibilité d'un accord. Et toute recherche de solution négociée dans un cadre de référence commun. C'est le cadre qui doit voler en éclats. C'est la loyauté qui doit voler en éclat. La volonté d'être aimable, de faire plaisir.

Il n'y a pas d'issue en dehors de la rupture et de la violence. Le moteur de cette interaction, le moyeu où convergent les signes de soumission et de domination, sa "prime cause" doivent être brisés. Le courtisan doit rompre ses liens d'une manière à ne les pouvoir renouer. C'est seulement avec les moyens de la force qu'il le pourra.

Cette exhortation à la rupture ne procède pas d'un quelconque désir de représailles, mais d'un constat facile à poser, que chacun fera en conscience : le prince ne peut être battu à son propre jeu.

Les règles qu'il suit sont volontairement tacites, réfractaires à la régulation, ondoyantes. Sa réticence à rendre transparentes les raisons de ses raisonnements jointe à son talent pour faire admettre la normalité de cette norme définit le contour de sa sphère d'influence.

Pour lui, contre toute logique, la formule d'établissement du savoir s'énonce de la façon suivante : s'il est bon de croire que X, alors X. Où X désigne une proposition ordinaire (par exemple : "La théorie littéraire est un réalisme symbolique.")

Or, ce genre de proposition n'est, rigoureusement parlant, ni vraie ni fausse, puisqu'en vertu de la procédure qui l'infère, elle n'a jamais été ni justifiée ni démontrée.

Elle n'est qu'une version possible --- indéterminée quant à sa valeur de vérité --- de la réalité, une version parmi d'autres à laquelle on veut croire sans examen attentif, réglé, sérieux, quoique pour des raisons utiles. Ce n'est donc pas le savoir, mais la norme d'accréditation du savoir elle-même qui est frauduleuse et non valide.

Ainsi, par un effet de boucle vicieux, tout X mis au crédit de son récit du monde conforte la légitimité du prince à faire croire que X. Cela témoigne d'un usage pragmatique de la croyance au sens le plus instrumentaliste du terme, d'un égoïsme pratique puisque, dans cette optique, n'est digne d'être cru que ce qui sert les intérêts du prince. Dans ces conditions, c'est-à-dire sans cadre métadiscursif, il est fortement inutile d'espérer un affrontement régulier, argumenté, raison contre raison.

Voilà pourquoi l'ultime recours du courtisan, c'est de se tenir à distance des procédés obliques employés par le prince, de quitter sans attendre les sous-sols où il manufacture, avec les petites mains de sa cour, les versions du monde qui siéront à son ambition personnelle. Quant à l'impunité du prince, qui a librement usé de moyens déloyaux dans l'unique but de faire croire à la vérité d'un talent purement spéculatif, il n'est pas de tribunal, pas d'instance qui puisse la lever.

En réalité, avant même d'être un sot, le bon courtisan --- celui qui ne se pose pas en rival --- est un individu qui souffre de croire, intellectuellement parlant, aux vertus de la déontologie dans un monde qui ne respecte pas les règles.

Ce point de vue éclaire rétrospectivement la sottise d'un jour nouveau. Il est insuffisant de la dépeindre comme une faiblesse morale, même si c'est aussi une lâcheté intellectuelle, en effet. Dire que la sottise se définit par le fait qu'elle renonce à se rallier à des justifications raisonnablement correctes est correct. Ajouter que la sottise est la manifestation d'une propension à croire à des raisons incorrectes est également correct. Mais le tableau est encore incomplet. La dernière touche n'a pas été posée.

Le trait de sottise qu'illustre le bon courtisan est déconcertant parce qu'il représente une démission épistémologique sincère. Le courtisan n'a pas renoncé intérieurement à son talent ou à ses raisons de croire, mais extérieurement. Extérieurement, il appartient à la famille des sots, pourtant cela n'en fait pas un "esprit boiteux", selon l'expression de Pascal. Extérieurement, le courtisan a abandonné sa prétention à faire valoir ses raisons de croire, mais il n'a pas abandonné ses raisons de croire. Il a renoncé à débattre publiquement de ses raisons. Il ne s'est pas abâtardi. Il ne s'est pas trahi. Ce n'est pas une défaite, c'est un repli. Un effacement.

En renonçant à la reconnaissance de soi, il disparaît du regard des autres et passe sous le radar des interactions sociales ; en contrepartie de quoi il préserve ses convictions.

Le courtisan qui soustrait ses convictions du débat public joue un jeu double qui peut se retourner contre lui s'il refuse catégoriquement, comme n'importe quel fraudeur, de les soumettre à des épreuves de vérité. Il faut alors constater que ce n'est pas l'épreuve de vérité que rejette le bon courtisan, mais le débat public, ce qui, objectivement, ne revient pas au même. Surtout si les règles garantissant l'équilibre et la succession des échanges sont impunément violées.

La démission du courtisan, ainsi réinterprétée, est le refus éthique de prendre part à un jeu dont les règles sont manipulées de telle sorte qu'il est impossible d'attirer l'attention sur, de dénoncer la manipulation elle-même. Il faut admettre que dans ce milieu vicié, prendre la parole, c'est (se) trahir. Lutter est impossible. Se taire un devoir. Sous condition d'oppression, tenir sa langue n'est jamais le choix de la facilité.

Tous les penseurs qui exaltent la communication, l'oral, la parole en acte comme foyer vivant de la pensée, tous les penseurs qui depuis Platon valorisent le verbe au détriment de l'écrit négligent --- contre leurs propres idéaux --- les effets de pouvoir inhérents à la maîtrise du discours que cette maîtrise soit pensée en termes d'éloquence, de rhétorique ou de style.

Car la seule question qui se pose quand quelqu'un parle n'est pas : qu'est-ce qu'il dit ? mais : qui est-il pour (se permettre de) dire ça ?

En vérité, le progrès de l'échange dialogué ne se mesure pas à la progression des thèmes du dialogue vers l'accord ou l'écart, du moins pas seulement. Il se mesure aussi à la résolution des liens de puissance sous-jacents qui travaillent à la distribution des rôles selon une logique différente de celles des idées.

Le débat oral est clivé en ce sens qu'il n'est jamais seulement ce qu'il est ou bien ce pour quoi il se donne ; il est irréductiblement cousu d'un sous-texte dont la fonction est d'établir qui est autorisé à dire ce qu'il dit. Chaque prise de parole est soumise à l'injonction implicite de formaliser les conditions auxquelles elle prétend prendre part.

L'espace, neutre, du débat démocratique est une fiction paresseuse. Souvenons-nous de la leçon de Lyotard : il n'y a pas de régime de phrases commun entre puissants et soumis. Et dans ce différend, le silence signale que quelque chose est nié.

Pourquoi le courtisan reste-t-il alors à la cour ? Ou plutôt pourquoi le courtisan diffère-t-il le moment de la quitter ? Ne lui est-il point besoin que d'un simple vouloir ?

Il faut croire que c'est parce que, abstraction faite des manœuvres de séduction du prince entreprises pour le fidéliser, indépendamment donc de ces manœuvres, le courtisan fait face à un dilemme. Ce dilemme est un classique de la rationalité économique connu sous le nom de "jeu de l'ultimatum", et dont la variante qui nous intéresse a été formulée jadis par La Bruyère.

Étant donné le jeu où il est pris, que fera le courtisan ? "S'éloignera-t-\[il de la cour\] avant d'en avoir tiré le moindre fruit, ou persistera-t-\[il\] à y demeurer sans grâces et sans récompenses ?" La recherche de l'optimum --- représentant le compromis maximal entre bénéfice (en faveurs) et perte (en autonomie) --- est de nature à différer dans le temps le point de rupture. Or, les économistes le savent, cet optimum dépend moins de la quantité escomptée, exprimée en valeur absolue, que de l'ampleur de la violation des normes de l'équité et de justesse, comme je vais l'expliquer.

On a vu certains princes répandre avec libéralité les avantages dont ils jouissaient en payant de copieux dividendes à leur cour ; on en a vu d'autres, à l'inverse, refuser d'accomplir le moindre des devoirs qui leur incombaient. Quelle que soit l'attitude du prince en termes d'octroi de faveurs, le courtisan trouve à se satisfaire. On peut dire, et c'est vrai, qu'il y a des courtisans plus mal logés que d'autres, de moins bien lotis. De plus malheureux. Mais il serait faux de croire que c'est le degré de magnanimité du prince, sa mansuétude, qui lui attache sa cour.

La question de savoir s'il faut s'éloigner de la cour ou y demeurer n'est pas rapportable à ce que fait ou ne fait pas le prince, elle est rapportable aux normes en vigueur dans le milieu où il le fait. Autrement dit, l'adhésion volontaire atteint sa valeur limite quand le courtisan compare les normes sociales du milieu où évolue le prince à celles d'autres milieux, que de cette comparaison il conclut à une violation du traitement de sa valeur, c'est-à-dire à une inégalité qui, elle, se mesure au principe --- non à la norme --- d'égalité. Plus le courtisan tardera à prendre conscience de la polysémie des normes, plus il reculera le moment de trancher son dilemme. Certains, d'ailleurs, ne le tranchent jamais.

# Conclusion

`\textsc{On aime croire}`{=tex} que le puissant impose ses décisions par force, et que ce faisant, il confisque la narration des faibles, des victimes, des laissés-pour-compte, des sans-voix.

On a tort, je crois.

La puissance commence au moment où il y a au centre le prince et autour, des spectateurs, au moment où un jeu sérieux s'instaure. Sans règles, un jeu qui ne dit pas son nom : l'un dit sa préférence ; les autres en veulent la raison. Ce n'est ni l'argent, ni la fonction, ni la naissance, ni l'érudition qui donne la puissance par effet de suite --- bien qu'indéniablement ils l'avantagent ---, c'est l'improvisation soudaine, sans tambour ni trompette, du grand jeu des préférences qui se dispute à plusieurs.

La logique sociale à l'œuvre dans les groupes comporte un moment structurel qui est le surgissement d'une voix qu'on écoute.

Toute la difficulté est là.

Cette voix qui "bruisse" prétend, affirme et juge sans démontrer, surgit sans prévenir. Les jugements de goûts, n'étant pas des arguments, se discutent peu, et ce peu de prise qu'ils offrent les rend glissants, fuyants, difficiles à manipuler, à contredire et donc à réfuter.

Il faut reconnaître que, si on est désormais en position d'expliquer comment à partir d'un coup d'éclat partiellement hasardeux, mais habilement exploité, se bâtit l'identité du prince, et comment il accède à une position dominante, on reste en revanche sous-équipé, en pratique, pour faire barrage à la construction de cet artifice ou pour le réformer.

S'il existait un lien causal entre ces événements, il y aurait alors une loi qui l'exprimerait ; et il serait possible de prédire l'apparition d'une telle concentration de pouvoir. Mais tel n'est pas le cas.

Il y a bien un ensemble de conditions congruentes, explicables a posteriori, qui offrent l'illusion d'un lien causal ; cependant l'émergence de la matrice de conditions permettant à un individu de cumuler autant d'avantages, de coaguler du pouvoir, provient d'une différenciation impossible à prédire. Il y a quelque chose comme un effet de seuil qui intervient.

Les conditions du succès de n'importe quel best-seller ou de n'importe quel tube musical sont identifiables, mais a posteriori. Il ne suffit pas de remplir ces conditions pour produire du succès.

Pourquoi ? Parce que la matrice des conditions du succès n'est pas régie par des lois causales.

Le succès d'un produit, tout comme le succès du prince (qui est aussi une sorte de produit), appartient à cette classe de phénomènes qui, surgissant dans des milieux aux propriétés identifiables, s'y distribuent de façon aléatoire. Trivialement, par exemple, l'apparition de grumeaux dans la pâte. Cette irréductible incertitude, qu'on ne saurait éliminer, semble plaider en faveur de l'idée qu'un ensemble de conditions ne prédétermine pas une forme d'ajustement.

Dans notre cas, l'identification des conditions d'apparition des structures d'influence ne permet pas d'identifier à l'avance et avec précision d'où surgira la concentration de talent, quel prince en herbe saisira l'avantage qu'il croit posséder pour le mettre en scène.

Ce n'est pas un défaut de la théorie. Hacking a montré que, face à ce type de structures, le mieux qu'on puisse faire, c'est d'œuvrer à leur "dévoilement ironique".

Jouer d'un effet de distanciation qui fait apparaître sous un angle dérisoire leur imposture ou, comme on disait encore à l'époque classique, leur "friponnerie".

C'est pour cette raison, par ironie donc, que j'ai voulu appeler ces charlatans des "princes".

# Glossaire

-   *Assentiment (figure de l')* : dans une économie de la valeur, cooptation inconditionnelle et simultanée des rôles hiérarchiques.
-   *Autorité* : prééminence d'un individu admise par un cercle donné.
-   *Bon courtisan* : courtisan qui abrite ses intérêts derrière le talent second du prince.
-   *Bruit* : propos soustraits à l'impératif de cohérence destinés à capter l'attention.
-   *Cercle* : dans un milieu, groupe qui fait corps autour d'un individu.
-   *Charisme* : le fait pour un individu de briller sans avoir à produire la preuve de son talent.
-   *Coup d'éclat* : réussite due au talent autant qu'à des circonstances favorables.
-   *Cour* : ensemble des individus adhérant à la légende du prince.
-   *Courtisan* : individu qui cherche un appui dans un milieu gouverné par une économie de la valeur.
-   *Courtisan-rival* : courtisan qui convoite la position du prince.
-   *Éclat* : mise en scène du soi idéalisé.
-   *Économie de la valeur* : système de distribution de la grandeur selon les normes dominantes du milieu où il s'applique.
-   *Épreuve de loyauté* : procédure consistant à tester le degré d'adhésion du courtisan.
-   *Épreuve de vérité* : procédure consistant à certifier la qualité d'un savoir.
-   *Idéologie* : capacité à imposer une interprétation du monde au détriment des récits alternatifs.
-   *Légende* : réputation acquise dans la mémoire collective par le coup d'éclat.
-   *Manières* : façons d'agir inconscientes renforçant l'éclat du prince.
-   *Prestige* : monnaie symbolique conférant une valeur publique.
-   *Prince* : individu qui use de son talent pour faire croire qu'il a du talent afin d'accroître sa valeur publique.
-   *Rang* : position hiérarchique au sein d'un groupe.
-   *Récit du monde* : système de croyances et de convictions imparfaitement cohérent produisant une interprétation du monde latente, quoique explicitable.
-   *Signal* : geste ou attitude attestant sans dissimulation possible l'existence d'une propriété.
-   *Sot* : type de personne qui consent à faire sien le jugement d'autrui.
-   *Talent avéré* : savoir-faire qualifié par une ou des épreuves de vérité.
-   *Talent de base* : curiosité investie devenue savoir-faire.
-   *Talent déclaré* : savoir-faire attribué par réputation.
-   *Talent second* : capacité à accroître de manière spéculative la valeur du talent de base.
-   *Techniques* : stratégies conscientes visant à exploiter au maximum le talent de base.
-   *Valeur publique* : grandeur ayant la reconnaissance d'un milieu.
-   *Récit officiel* : interprétation du monde circulant dans un cercle où elle fait foi.

# Références

```{=tex}
\begin{hangparas}{2em}{1}
\setlength{\parindent}{0em}
```
B`\textsc{ergounioux}`{=tex} Pierre, *Enfantillages*, Paris, Carnets de l'Herne, 2019.

C`\textsc{astiglione}`{=tex} Baldassare, *Le Livre du courtisan* (1528), Paris, Garnier-Flammarion, 1999.

D`\textsc{errida}`{=tex} Jacques, *Force de loi*, Paris, Galilée, 1994.

E`\textsc{lias}`{=tex} Norbert, *La Société de cour* (1969), Paris, Champs Flammarion, 1985.

E`\textsc{ngel}`{=tex} Pascal, *Les Vices du savoir : Essai d'éthique intellectuelle*, Paris, Agone, 2019.

F`\textsc{rankfurt}`{=tex} Harry, *De l'art de dire des conneries* (2005), Paris, 10/18, 2006.

G`\textsc{offman}`{=tex} Erwin, *La Mise en scène de la vie quotidienne* (1956), Paris, Minuit, 1996.

H`\textsc{acking}`{=tex} Ian, *Entre science et réalité : La construction sociale de quoi ?* (2001), Paris, La Découverte, 2008.

H`\textsc{einich}`{=tex} Nathalie, *Des valeurs : Une approche sociologique*, Paris, Gallimard, 2017.

K`\textsc{ojève}`{=tex} Alexandre, *La Notion de l'autorité* (1942), Paris, Gallimard, 2004.

L`\textsc{a}`{=tex} B`\textsc{ruyère}`{=tex} Jean de, *Caractères* (1688), Paris, Gallimard, 1975.

L`\textsc{yotard}`{=tex} Jean-François, *Le Différend*, Paris, Minuit, 1983.

M`\textsc{enger}`{=tex} Pierre-Michel, *Le Travail créateur : S'accomplir dans l'incertain*, Paris, Points essais, 2014.

N`\textsc{ewman}`{=tex} John Henry, *Grammaire de l'assentiment* (1870), Paris, Ad Solem, 2010.

O`\textsc{riggi}`{=tex} Gloria, *La Réputation*, Paris, PUF, 2015.

P`\textsc{ascal}`{=tex} Blaise, *De l'esprit géométrique ; entretien avec M. de Sacy ; écrits sur la grâce et autres textes* (1658), Paris, Garnier-Flammarion, 1985.

R`\textsc{evauld}`{=tex} D'A`\textsc{llones}`{=tex} Myriam, *Le Pouvoir des commencements : Essai sur l'autorité*, Seuil, 2006.

S`\textsc{chaeffer}`{=tex} Jean-Marie, *Théorie des signaux coûteux, esthétique et art*, Rimouski, Tangence, 2009.

S`\textsc{chopenhauer}`{=tex} Arthur, *L'Art d'avoir toujours raison, suivi de la lecture et Les livres et Penseurs personnels* (1831), Paris, J'ai Lu, 2016.

S`\textsc{chotté}`{=tex} Manuel, "Le Don, le génie et le talent", *Genèses*, *93*(4), 2013.

S`\textsc{perber}`{=tex} Dan, "The Guru Effect", *Review of Philosophy and Psychology*, *1*(4), 2010.

S`\textsc{tarobinski}`{=tex} Jean, *Le Remède dans le mal : Critique et légitimation de l'artifice à l'âge des Lumières*, Paris, Gallimard, 1989.

```{=tex}
\end{hangparas}
\setlength{\parindent}{1em}
```
