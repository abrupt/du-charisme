# ~/ABRÜPT/BERNARD BOURRIT/DU CHARISME/*

La [page de ce livre](https://abrupt.cc/bernard-bourrit/du-charisme/) sur le réseau.

## Sur le livre

Tout part d'une idée simple. La *Black Box Theory*.<br>
Prenez une boîte.<br>
On sait ce qui entre, ce qui sort. Pas ce qu'il y a dedans.<br>
Dedans, faites croire qu'il y a de la valeur. Une valeur quelconque, désirable. Pour ça, il y a des moyens. La curiosité aide.<br>
Exagérez, au besoin, la valeur de cette valeur. N'hésitez pas à nourrir un orgueil de propriétaire, à faire des envieux. Recrutez-les, s'ils font défaut.<br>
Mais surtout n'ouvrez pas la boîte. En aucune manière.<br>
Ça risquerait de se voir sinon.<br>
Il n'y a rien.

## Sur l'auteur

Bernard Bourrit, né en 1977, vit à Genève. Il est l’auteur de textes brefs sur l’art brut, les portraits funéraires, les reliques, les doubles dévorants, le scepticisme, l'anarchisme, la photographie documentaire, les arbres. Publié dans les revues *Critique*, *La Part de l’Œil*, *L’Homme*, *Revue d’histoire des religions*, *The Black Herald*, *Poétique*, *Littérature*, il est l’auteur de *Fautrier ou le désengagement de l’art* (L’Épure, 2006), *Montaigne. Pensées frivoles et vaines écorces* (Le Temps qu’il fait, 2018) et co-traducteur de Zheng Yi (Bleu de Chine, 2007), Tsering Woeser, Ge Fei et Jia Pingwa (Gallimard, 2010, 2012, 2017).

## Sur la licence

Cet [antilivre](https://abrupt.cc/antilivre/) est disponible sous licence Creative Commons Attribution – Pas d’Utilisation Commerciale – Partage dans les Mêmes Conditions 4.0 International (CC-BY-NC-SA 4.0).

## Etc.

Vous pouvez également découvrir notre [site](https://abrupt.cc) pour davantage d'informations sur notre démarche, notamment quant au [partage de nos textes](https://abrupt.cc/partage/).
